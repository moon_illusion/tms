package org.aid.commons;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

public class SessionTimeoutFilter implements Filter {
	private static PathMatcher pathMatcher = new AntPathMatcher();
	private String[] ignoredUrls;

	public String[] getIgnoredUrls() {
		return ignoredUrls;
	}

	public void setIgnoredUrls(String[] ignoredUrls) {
		this.ignoredUrls = ignoredUrls;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String urlstr = filterConfig.getInitParameter("IGNORED_URLS");
		
		if(urlstr != null){
			this.setIgnoredUrls(urlstr.split(","));
		}
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
			ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		if (needIntercept(request)) {
			HttpSession session = request.getSession();
			SecurityContext securityContext = (SecurityContext)session.getAttribute("SPRING_SECURITY_CONTEXT");
			if(securityContext == null){
				// 如果是ajax请求响应头会有，x-requested-with；
				if (request.getHeader("X-Requested-With") != null
						&& request.getHeader("X-Requested-With").equalsIgnoreCase("XMLHttpRequest")) {

					// 在响应头设置session状态
					response.setHeader("sessionstatus", "timeout");
					return;
				}
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		this.ignoredUrls = null;
	}
	
	private boolean needIntercept(HttpServletRequest request) {
		String url = request.getRequestURI();
		String base = request.getContextPath();

		if (this.ignoredUrls != null) {
			for (String u : ignoredUrls) {
				if (u != null && pathMatcher.match(base + u, url)) {
					return false;
				}
			}
		}

		return true;
	}
}
