<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="path" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>教师信息管理系统</title>
	
	<link rel="stylesheet" type="text/css" href="${path}/resources/framework/easyui/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="${path}/resources/framework/easyui/themes/icon.css"/>
	
	<script type="text/javascript" src="${path}/resources/framework/easyui/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="${path}/resources/framework/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${path}/resources/framework/easyui/locale/easyui-lang-zh_CN.js"></script>
	
	<script type="text/javascript" src="${path}/resources/js/jquery.json-2.3.min.js"></script>
</head>
<body>
	<div class="easyui-window" style="width: 400px;height: 270px;" 
		data-options="title:'请登录',collapsible:false,minimizable:false,maximizable:false,closable:false">
		<form action="${path}/<c:url value='j_spring_security_check'/>" method="post" style="padding:10px 20px 10px 60px;">
			<p><c:if test="${not empty param.error}"><span style="text-align: left;"><font color="red">登录失败，原因${SPRING_SECURITY_LAST_EXCEPTION}.</font></span></c:if></p>
			
			<p style="font-size: 13px;font-weight: bold;">用户名:</p>
			<p><input class="borderHighlight" type="text"  name='j_username' style="width:250px;" 
				value='<c:if test="${not empty param.error}"><c:out value="${SPRING_SECURITY_LAST_USERNAME}"/></c:if>' /></p>
            <p style="font-size: 13px;font-weight: bold;">密码:</p>
            <p><input class="borderHighlight" type="password" name='j_password' style="width:250px;" /></p>
            <p>
              <input value="登录" type="submit" class="btnBlue" style="width:255px; width:257px\9;" />
          </p>
		</form>
	</div>
</body>
</html>