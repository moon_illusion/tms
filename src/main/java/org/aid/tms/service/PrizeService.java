package org.aid.tms.service;


import org.aid.tms.dao.hibernate.BaseDao;
import org.aid.tms.dao.hibernate.PrizeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrizeService extends BaseService {
	
	@Autowired
	private PrizeDao prizeDao;
	
	@Override
	public BaseDao getBaseDao() {
		return prizeDao;
	}

	@Override
	public String getObjName() {
		return "获奖记录";
	}

}
