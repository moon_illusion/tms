package org.aid.tms.dao.hibernate;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.aid.commons.IPagination;
import org.aid.tms.constants.Relation;
import org.aid.tms.vo.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

public abstract class BaseDao<T,TID extends Serializable> extends BaseHibernateDao<T, TID> {

	public IPagination<T> getPageList(Integer page,Integer rows,List<QueryParam> queryParams) {
		DetachedCriteria criteria = getCriteriaByQeuryParams(queryParams);
		return findByCriteria(criteria, page, rows);
	}
	
	@Override
	abstract protected Class<T> getEntityClass();

	public List<?> getList(List<QueryParam> queryParams) {
		DetachedCriteria criteria = getCriteriaByQeuryParams(queryParams);
		return findByCriteria(criteria);
	}
	
	private DetachedCriteria getCriteriaByQeuryParams(List<QueryParam> queryParams){
		DetachedCriteria criteria = DetachedCriteria.forClass(getEntityClass());
		if(queryParams != null){
			for(QueryParam param : queryParams){
				String k = param.getName();
				Object v = param.getValue();
				String r = param.getRelation();
				String t = param.getType();
				
				if( StringUtils.isNotEmpty(r) && v != null){
					if(StringUtils.isNotEmpty(t)){
						k = t + "." + k;
					}
					Criterion criterion = Restrictions.sqlRestriction(k + " " + r + " " + v);
					if(v instanceof String){
						String d = StringUtils.trim((String)v);
						if(d.matches("\\d{4}-\\d{2}-\\d{2}")){
							criterion = Restrictions.sqlRestriction(k + " " + r + " " + getDate(d, "yyyy-MM-dd"));
						}else if(Relation.LIKE.equals(r)){
							criterion = Restrictions.like(k, d,MatchMode.ANYWHERE);
						}else{
							criterion = Restrictions.sqlRestriction(k + " " + r + " '" + v + "'");
						}
					}
					
					criteria.add(criterion);
				}
			}
		}
		return criteria;
	}
	
	private Date getDate(String source,String pattern){
		try {
			return new SimpleDateFormat(pattern).parse(source);
		} catch (ParseException e) {}
		return null;
	}
}
