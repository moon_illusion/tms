<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north'"
		style="height: 70px; vertical-align: middle; overflow: hidden;">
		<dl id="topic_query_form" class="query-form">
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<dt>姓名</dt> 
			<dd>
				<select name="user_id" style="width: 110px;" _param="type:'this_'">
					<option value=""></option>
					<c:forEach items="${users}" var="user">
						<option value="${user.userId}">${user.fullName}</option>
					</c:forEach>
				</select>
			</dd>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user_id" _param="type:'this_'" value="${users[0].userId}" />
			</security:authorize>
			<dt>项目名称</dt>
			<dd>
				<input type="text" name="topicName" id="topicName" _param="relation:'like'" />
			</dd>
			<dt <security:authorize ifAllGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >项目来源</dt> 
			<dd><input type="text" name="projectSource" _param="relation:'like'" /></dd>
			<dt <security:authorize ifNotGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >年度</dt> 
			<dd>
				<select name="year" style="width: 110px;">
					<option value=""></option>
					<c:forEach begin="2010" end="2020" var="item">
						<option value="${item}">${item}</option>
					</c:forEach>
				</select>
			</dd>
			<dd>
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="queryTopic()">查询</a>
			<dd>
		</dl>
	</div>
	<div data-options="region:'center'">
		<table id="topic_grid" class="easyui-datagrid"
			data-options="
				method:'post',
				url:'${path}/topic/list',
				fit:true,
				border:false,
				singleSelect:true,
				idField:'topicId',
				pagination:true,
				toolbar:'#topic_grid_toolbar',
				<security:authorize ifNotGranted="ROLE_ADMIN">
				queryParams:{
					queryParams:$.toJSON([{
						name:'user_id',
						type:'this_',
						relation:'=',
						value:'${users[0].userId}'
					}])
				},
				</security:authorize>
				onDblClickRow:function(rowIndex, rowData){
					showModifyTopicDialog(rowData);
				}
			">
			<thead>
				<tr>
					<th data-options="field:'user',width:100,align:'center',
						formatter:function(value,row,index){
							if(value){
								return value.fullName;
							}
							return '';
						}">姓名</th>
					<th data-options="field:'topicName',width:150,align:'center'">课题名称</th>
					<th data-options="field:'beginTime',width:160,align:'center',
						formatter:function(value,row,index){
							if(row){
								return value + '—' + row.endTime;
							}
							return value + '至今';
						}
					">起止年月</th>
					<th data-options="field:'funds',width:150,align:'center'">经费</th>
					<th data-options="field:'projectSource',width:150,align:'center'">项目来源</th>
					<th data-options="field:'presenter',width:130,align:'center'">主持人</th>
					<th data-options="field:'partner',width:130,align:'center'">合作人</th>
					<th data-options="field:'year',width:100,align:'center'">年度</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div id="topic_grid_toolbar">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="showAddTopicDialog()">新增</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="showModifyTopicDialog()">修改</a>
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteTopic()">删除</a>
	<a href="#" class="easyui-menubutton" data-options="menu:'#mmtopic'">导出</a>
</div>

<div id="topic_detail" class="easyui-dialog" style="width:420px;height:380px;padding:10px;"
	data-options="closed:true,cache:false,modal:true,buttons:'#topic_detail_buttons'">
	<form id="topic_detail_form" method="post">
	<input type="hidden" name="topicId" />
	<dl class="detail-form-list">
		<dt>姓名：</dt> 
		<dd>
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<select name="user" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach items="${users}" var="user">
					<option value="${user.userId}">${user.fullName}</option>
				</c:forEach>
			</select>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user" value="${users[0].userId}"/>
				<input type="text" name="fullName" readonly="readonly" value="${users[0].fullName}"/>
			</security:authorize>
		</dd>
		<dt>课题名称：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="topicName" data-options="required:true"/></dd>
		<dt>开始年月：</dt> 
		<dd><input type="text" class="easyui-datebox" name="beginTime" data-options="required:true"/></dd>
		<dt>结束年月：</dt>
		<dd><input type="text" class="easyui-datebox" name="endTime" data-options="required:true"/></dd>
		<dt>经费：</dt> 
		<dd><input type="text" class="easyui-numberbox" name="funds" data-options="required:true"/></dd>
		<dt>项目来源：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="projectSource" data-options="required:true"/></dd>
		<dt>主持人：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="presenter" data-options="required:true"/></dd>
		<dt>合作人：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="partner" data-options="required:true"/></dd>
		<dt>年度：</dt> 
		<dd>
			<select name="year" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach begin="2010" end="2020" var="item">
					<option value="${item}">${item}</option>
				</c:forEach>
			</select>
		</dd>
	</dl>
	<input type="hidden" name="createTime" />
	<input type="hidden" name="createUsername" />
</form>
	
</div>

<div id="topic_detail_buttons">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveTopic()">保存</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#topic_detail').dialog('close');">取消</a>
</div>

<div id="mmtopic" data-options="onClick:menuTopicHandler" style="width:150px;">
	<div data-options="name:'excel'">Excel</div>
</div>

<form id="topic_export_form" method="post" action="${path}/topic/export/excel">
	<security:authorize ifAllGranted="ROLE_ADMIN">
		<input type="hidden" id="topic_query_params" name="queryParams">
	</security:authorize>
	<security:authorize ifNotGranted="ROLE_ADMIN">
		<input type="hidden" id="topic_query_params" name="queryParams" value='[{"name":"user_id","type":"this_","relation":"=","value":"${users[0].userId}"}]'>
	</security:authorize>
</form>

<script type="text/javascript">
function menuTopicHandler(item){
	if(item.name=='excel'){
		$('#topic_export_form').submit();
	}
}
	
function queryTopic() {
	var params = [];
	
	$(':input','#topic_query_form').each(function(i,v){
		var self = $(this);
		var val = self.val();
		if(val && val!='' ){
			var param = eval('({' + (self.attr('_param') || '') + '})') || {};
			if(!param.name || param.name==''){
				param.name = self.attr('name');
			}
			if(!param.relation || param.relation == ''){
				param.relation = '=';
			}
			param.value = val;
			params.push(param);
		}
	});
	
	var _query_params = $.toJSON(params);;
	
	$('#topic_query_params').val(_query_params);
	$('#topic_grid').datagrid('options').queryParams.queryParams =_query_params;
	$('#topic_grid').datagrid('reload');
}

function showAddTopicDialog() {
	$("#topic_detail")
		.dialog('setTitle', '新增课题')
		.dialog('open');
	$('#topic_detail_form').form('clear');
	$('select','#topic_detail_form').each(function(i,v){
		$(this).val($('option',this)[0].value);
	});
	$('input[name="user"]','#topic_detail_form').val('${users[0].userId}');
	$('input[name="fullName"]','#topic_detail_form').val('${users[0].fullName}');
}

function showModifyTopicDialog(rowData) {
	if (!rowData) {
		var row = $('#topic_grid').datagrid('getSelected');
		if (row) {
			rowData = row;
		} else {
			$.alert('请选择要修改的记录！');
		}
	}

	if(rowData){
		$("#topic_detail")
			.dialog('setTitle', '修改课题')
			.dialog('open');
		var url = '${path}/topic/detail?topicId=' + rowData.topicId;
		$.get(url,function(rs){
			if(rs){
				var topic = rs;
				var user = rs.user;
				if(user){
					topic.user = user.userId;
					topic.fullName = user.fullName;
					$('#topic_detail_form').form('load',topic);
				}
			}
		},'json');
	}
}

/**
 * 
 */
function saveTopic() {
	var url = '${path}/topic/save'
	
	$('#topic_detail_form').form('submit', {
		url : url,
		onSubmit : function() {
			if($('input[name="user"]','#topic_detail_form').val() == -1){
				return false;
			}
			if($('input[name="year"]','#topic_detail_form').val() == -1){
				return false;
			}
			return $('#topic_detail_form').form('validate');
		},
		success : function(data) {
			if(data){
				$('#topic_detail').dialog('close');
				$('#topic_grid').datagrid('reload');
				var rs = $.evalJSON(data);
				$.message(rs.msg);
			}
		}
	});
}

/**
 * 
 */
function deleteTopic() {
	var row = $('#topic_grid').datagrid('getSelected');
	var index = $('#topic_grid').datagrid('getRowIndex', row);
	if (row) {
		$.confirm('请确认是否删除选择的记录?', function() {
			var url = '${path}/topic/delete';
			$.post(url, {
				topicId : row.topicId
			},
			function(rs){
				if(rs.success){
					$.message(rs.msg || '删除课题成功！');
					$('#topic_grid').datagrid('reload');
				}else{
					$.message(rs.msg || '删除课题失败！');
				}
			},'json');
		});
	} else {
		$.alert('请选择要删除的记录！');
	}
}
</script>