<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north'"
		style="height: 70px; vertical-align: middle; overflow: hidden;">
		<dl id="resume_query_form" class="query-form">
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<dt>姓名</dt> 
			<dd>
				<select name="user_id" style="width: 110px;" _param="type:'this_'">
					<option value=""></option>
					<c:forEach items="${users}" var="user">
						<option value="${user.userId}">${user.fullName}</option>
					</c:forEach>
				</select>
			</dd>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user_id" _param="type:'this_'" value="${users[0].userId}" />
			</security:authorize>
			<dt>工作单位</dt>
			<dd>
				<input type="text" name="danwei" id="danwei" _param="relation:'like'" />
			</dd>
			<dt <security:authorize ifAllGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >职称</dt> 
			<dd><input type="text" name="title" _param="relation:'like'" /></dd>
			<dt <security:authorize ifNotGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >年度</dt> 
			<dd>
				<select name="year" style="width: 110px;">
					<option value=""></option>
					<c:forEach begin="2010" end="2020" var="item">
						<option value="${item}">${item}</option>
					</c:forEach>
				</select>
			</dd>
			<dd>
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="queryResume()">查询</a>
			<dd>
		</dl>
	</div>
	<div data-options="region:'center'">
		<table id="resume_grid" class="easyui-datagrid"
			data-options="
				method:'post',
				url:'${path}/resume/list',
				fit:true,
				border:false,
				singleSelect:true,
				idField:'resumeId',
				pagination:true,
				toolbar:'#resume_grid_toolbar',
				<security:authorize ifNotGranted="ROLE_ADMIN">
				queryParams:{
					queryParams:$.toJSON([{
						name:'user_id',
						type:'this_',
						relation:'=',
						value:'${users[0].userId}'
					}])
				},
				</security:authorize>
				onDblClickRow:function(rowIndex, rowData){
					showModifyResumeDialog(rowData);
				}
			">
			<thead>
				<tr>
					<th data-options="field:'user',width:100,align:'center',
						formatter:function(value,row,index){
							if(value){
								return value.fullName;
							}
							return '';
						}">姓名</th>
					<th data-options="field:'beginTime',width:170,align:'center',
						formatter:function(value,row,index){
							if(row){
								return value + '—' + row.endTime;
							}
							return value + '至今';
						}
					">起止年月</th>
					<th data-options="field:'danwei',width:150,align:'center'">工作单位</th>
					<th data-options="field:'post',width:150,align:'center'">职务</th>
					<th data-options="field:'title',width:150,align:'center'">职称</th>
					<th data-options="field:'year',width:100,align:'center'">年度</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div id="resume_grid_toolbar">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="showAddResumeDialog()">新增</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="showModifyResumeDialog()">修改</a>
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteResume()">删除</a>
	<a href="#" class="easyui-menubutton" data-options="menu:'#mmresume'">导出</a>
</div>

<div id="resume_detail" class="easyui-dialog" style="width:420px;height:380px;padding:10px;"
	data-options="closed:true,cache:false,modal:true,buttons:'#resume_detail_buttons'">
	<form id="resume_detail_form" method="post">
	<input type="hidden" name="resumeId" />
	<dl class="detail-form-list">
		<dt>姓名：</dt> 
		<dd>
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<select name="user" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach items="${users}" var="user">
					<option value="${user.userId}">${user.fullName}</option>
				</c:forEach>
			</select>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user" value="${users[0].userId}"/>
				<input type="text" name="fullName" readonly="readonly" value="${users[0].fullName}"/>
			</security:authorize>
		</dd>
		<dt>开始年月：</dt> 
		<dd><input type="text" class="easyui-datebox" name="beginTime" data-options="required:true"/></dd>
		<dt>结束年月：</dt> 
		<dd><input type="text" class="easyui-datebox" name="endTime" data-options="required:true"/></dd>
		<dt>工作单位：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="danwei" data-options="required:true"/></dd>
		<dt>职务：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="post" data-options="required:true"/></dd>
		<dt>职称：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="title" data-options="required:true"/></dd>
		<dt>年度：</dt> 
		<dd>
			<select name="year" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach begin="2010" end="2020" var="item">
					<option value="${item}">${item}</option>
				</c:forEach>
			</select>
		</dd>
		<dt>在各级学术单位任职情况：</dt> 
		<dd><textarea name="postHistory" rows="3" cols="25"></textarea></dd>
	</dl>
	<input type="hidden" name="createTime" />
	<input type="hidden" name="createUsername" />
</form>
	
</div>

<div id="resume_detail_buttons">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveResume()">保存</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#resume_detail').dialog('close');">取消</a>
</div>

<div id="mmresume" data-options="onClick:menuResumeHandler" style="width:150px;">
	<div data-options="name:'excel'">Excel</div>
</div>

<form id="resume_export_form" method="post" action="${path}/resume/export/excel">
	<security:authorize ifAllGranted="ROLE_ADMIN">
		<input type="hidden" id="resume_query_params" name="queryParams">
	</security:authorize>
	<security:authorize ifNotGranted="ROLE_ADMIN">
		<input type="hidden" id="resume_query_params" name="queryParams" value='[{"name":"user_id","type":"this_","relation":"=","value":"${users[0].userId}"}]'>
	</security:authorize>
</form>

<script type="text/javascript">

function menuResumeHandler(item){
	if(item.name=='excel'){
		$('#resume_export_form').submit();
	}
}
	
function queryResume() {
	var params = [];
	
	$(':input','#resume_query_form').each(function(i,v){
		var self = $(this);
		var val = self.val();
		if(val && val!='' ){
			var param = eval('({' + (self.attr('_param') || '') + '})') || {};
			if(!param.name || param.name==''){
				param.name = self.attr('name');
			}
			if(!param.relation || param.relation == ''){
				param.relation = '=';
			}
			param.value = val;
			params.push(param);
		}
	});
	
	var _query_params = $.toJSON(params);;
	
	$('#resume_query_params').val(_query_params);
	$('#resume_grid').datagrid('options').queryParams.queryParams =_query_params;
	$('#resume_grid').datagrid('reload');
}

function showAddResumeDialog() {
	$("#resume_detail")
		.dialog('setTitle', '新增简历')
		.dialog('open');
	$('#resume_detail_form').form('clear');
	$('select','#resume_detail_form').each(function(i,v){
		$(this).val($('option',this)[0].value);
	});
	$('input[name="user"]','#resume_detail_form').val('${users[0].userId}');
	$('input[name="fullName"]','#resume_detail_form').val('${users[0].fullName}');
}

function showModifyResumeDialog(rowData) {
	if (!rowData) {
		var row = $('#resume_grid').datagrid('getSelected');
		if (row) {
			rowData = row;
		} else {
			$.alert('请选择要修改的记录！');
		}
	}

	if(rowData){
		$("#resume_detail")
			.dialog('setTitle', '修改简历')
			.dialog('open');
		var url = '${path}/resume/detail?resumeId=' + rowData.resumeId;
		$.get(url,function(rs){
			if(rs){
				var resume = rs;
				var user = rs.user;
				if(user){
					resume.user = user.userId;
					resume.fullName = user.fullName;
					$('#resume_detail_form').form('load',resume);
				}
			}
		},'json');
	}
}

/**
 * 
 */
function saveResume() {
	var url = '${path}/resume/save'
	
	$('#resume_detail_form').form('submit', {
		url : url,
		onSubmit : function() {
			if($('input[name="user"]','#resume_detail_form').val() == -1){
				return false;
			}
			if($('input[name="year"]','#resume_detail_form').val() == -1){
				return false;
			}
			return $('#resume_detail_form').form('validate');
		},
		success : function(data) {
			if(data){
				$('#resume_detail').dialog('close');
				$('#resume_grid').datagrid('reload');
				var rs = $.evalJSON(data);
				$.message(rs.msg);
			}
		}
	});
}

/**
 * 
 */
function deleteResume() {
	var row = $('#resume_grid').datagrid('getSelected');
	var index = $('#resume_grid').datagrid('getRowIndex', row);
	if (row) {
		$.confirm('请确认是否删除选择的记录?', function() {
			var url = '${path}/resume/delete';
			$.post(url, {
				resumeId : row.resumeId
			},
			function(rs){
				if(rs.success){
					$.message(rs.msg || '删除简历成功！');
					$('#resume_grid').datagrid('reload');
				}else{
					$.message(rs.msg || '删除简历失败！');
				}
			},'json');
		});
	} else {
		$.alert('请选择要删除的记录！');
	}
}
</script>