package org.aid.tms.dao.hibernate;

import org.aid.tms.model.TPrize;
import org.springframework.stereotype.Repository;

@Repository
public class PrizeDao extends BaseDao<TPrize, Integer> {

	@Override
	protected Class<TPrize> getEntityClass() {
		return TPrize.class;
	}
}