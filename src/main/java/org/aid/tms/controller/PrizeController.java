package org.aid.tms.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aid.commons.IPagination;
import org.aid.commons.LoginUserManger;
import org.aid.tms.model.TEducation;
import org.aid.tms.model.TPrize;
import org.aid.tms.model.TUser;
import org.aid.tms.service.BaseService;
import org.aid.tms.service.PrizeService;
import org.aid.tms.service.ServiceException;
import org.aid.tms.service.UserService;
import org.aid.tms.vo.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/prize")
public class PrizeController extends BaseController{
	@Autowired
	private PrizeService prizeService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/view")
	public String goPrizePage(SecurityContextHolderAwareRequestWrapper request,Model model){
		String username = null;
		if(!request.isUserInRole("ROLE_ADMIN")){
			username = LoginUserManger.getLoginUsername();
		}
		List<TUser> users = userService.getUsersByUsername(username);
		model.addAttribute("users", users);
		return "prize";
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> getPrizeList(Integer page,Integer rows,String queryParams){
		Map<String,Object> results = new HashMap<String, Object>();
		QueryParam[] params = null;
		if(StringUtils.isNotEmpty(queryParams)){
			try {
				params = new ObjectMapper().readValue(queryParams, QueryParam[].class);
			} catch (Throwable e) {
				System.out.println(e.getMessage());
			} 
		}
		IPagination<?> pagination = prizeService.getListByPage(page,rows,params==null?null:Arrays.asList(params));
		
		results.put("total", pagination.getTotalCount());
		results.put("rows", pagination.getResults());
		return results;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public Map<String,Object> savePrize(TPrize prize){
		Map<String,Object> results = new HashMap<String, Object>();
		if(prize != null){
			if(prize.getPrizeId() == null){
				prize.setCreateTime(new Timestamp(new Date().getTime()));
				prize.setCreateUsername(LoginUserManger.getLoginUsername());
			}else{
				prize.setLastUpdateTime(new Timestamp(new Date().getTime()));
				prize.setLastUpdateUsername(LoginUserManger.getLoginUsername());
			}
		}
		
		try{
			prizeService.save(prize);
			results.put("success", true);
			results.put("msg","保存获奖纪录成功");
		}catch(ServiceException e){
			results.put("msg","保存获奖纪录失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	public Object getPrize(Integer prizeId){
		return prizeService.getById(prizeId);
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String,Object> deletePrizeById(Integer prizeId){
		
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			prizeService.deleteById(prizeId);
			results.put("success", true);
			results.put("msg","删除获奖纪录成功");
		}catch(ServiceException e){
			results.put("msg","删除获奖纪录失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/export/excel")
	public void exportPrizeList(HttpServletRequest request,HttpServletResponse response,String queryParams) throws IOException{
		exportExcel(request, response, queryParams, new BuildExcelCallback() {
			
			@Override
			public BaseService getService() {
				return prizeService;
			}
			
			@Override
			public String getFileName() {
				return "获得情况";
			}
			
			@Override
			public String[] getColumnHeads() {
				return new String[]{"姓名","奖项名称","项目名称","主持人","和作者","时间","授予单位","年度"};
			}

			@Override
			public void fillRowData(HSSFRow row, Object d,CellStyle baseStyle) {
				TPrize p = (TPrize)d;
				setCellValue(row,0,p.getUser().getFullName(),baseStyle);
				setCellValue(row,1,p.getPrizeName(),baseStyle);
				setCellValue(row,2,p.getPrizeProject(),baseStyle);
				setCellValue(row,3,p.getPresenter(),baseStyle);
				setCellValue(row,4,p.getPartner(),baseStyle);
				setCellValue(row,5,p.getPrizeTime());
				setCellValue(row,6,p.getDanwei(),baseStyle);
				setCellValue(row,7,p.getYear(),baseStyle);
			}
		});
	}
}
