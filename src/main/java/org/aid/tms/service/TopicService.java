package org.aid.tms.service;

import org.aid.tms.dao.hibernate.BaseDao;
import org.aid.tms.dao.hibernate.TopicDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicService extends BaseService {
	
	@Autowired
	private TopicDao topicDao;
	
	@Override
	public BaseDao getBaseDao() {
		return topicDao;
	}

	@Override
	public String getObjName() {
		return "课题";
	}
}
