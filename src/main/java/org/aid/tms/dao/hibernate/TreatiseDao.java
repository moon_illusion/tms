package org.aid.tms.dao.hibernate;

import org.aid.tms.model.Treatise;
import org.springframework.stereotype.Repository;

@Repository
public class TreatiseDao extends BaseDao<Treatise, Integer> {

	@Override
	protected Class<Treatise> getEntityClass() {
		return Treatise.class;
	}
}
