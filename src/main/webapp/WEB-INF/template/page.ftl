<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north'"
		style="height: 40px; vertical-align: middle; overflow: hidden;">
		<dl id="user_query_form" class="query-form">
			<dt>用户名:</dt>
			<dd>
				<input type="text" name="username" id="username" />
			</dd>
			<dt>姓名:</dt>
			<dd>
				<input type="text" name="fullName" id="fullName" />
			</dd>
			<dd>
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="query()">查询</a>
			<dd>
		</dl>
	</div>
	<div data-options="region:'center'">
		<table id="user_grid" class="easyui-datagrid"
			data-options="
				method:'post',
				url:'${path}/user/list.json',
				fit:true,
				border:false,
				singleSelect:true,
				idField:'userId',
				pagination:true,
				toolbar:'#user_grid_toolbar',
				onDblClickRow:function(rowIndex, rowData){
					showModifyDialog(rowData);
				}
			">
			<thead>
				<tr>
					<th data-options="field:'username',width:100,align:'center'">用户名</th>
					<th data-options="field:'fullName',width:100,align:'center'">姓名</th>
					<th data-options="field:'danwei',width:100,align:'center'">所在单位</th>
					<th data-options="field:'zhuanye',width:100,align:'center'">所属专业</th>
					<th data-options="field:'sex',width:100,align:'center',
						formatter:function(value,row,index){
							if(value == 1){
								return '男';
							}
							if(value == 2){
								return '女';
							}
							return '其他';
						}">性别</th>
					<th data-options="field:'age',width:100,align:'center'">年龄</th>
					<th data-options="field:'phone',width:100,align:'center'">手机号</th>
					<th data-options="field:'email',width:100,align:'center'">Email</th>
					<th data-options="field:'role',width:100,align:'center',
						formatter:function(value,row,index){
							if(value == 1){
								return '管理员';
							}
							return '普通用户';
						}">
					角色
					</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div id="user_grid_toolbar">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="showAddDialog()">新增</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="showModifyDialog()">修改</a>
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteRow()">删除</a>
</div>

<div id="user_detail" class="easyui-dialog" style="width:480px;height:420px;padding:10px;"
	data-options="closed:true,cache:false,modal:true,buttons:'#detail_buttons'">
	<form id="user_detail_form" method="post">
	<input type="hidden" name="userId" />
	<dl class="detail-form-list">
		<dt>用户名：</dt> 
		<dd>
			<input type="text" class="easyui-validatebox" name="username" data-options="required:true,validType:'length[3,30]'" />
		</dd>
		<dt>密码：</dt>
		<dd><input type="password" class="easyui-validatebox" name="password" data-options="required:true" /></dd>
		<dt>姓名：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="fullName" data-options="required:true"/></dd>
		<dt>性别：</dt> 
		<dd>
			<select type="text" name="sex" style="width: 155px;">
				<option value="1">男</option>
				<option value="2">女</option>
				<option value="3">其他</option>
			</select>
		</dd>
		<dt>年龄：</dt> 
		<dd><input type="text" class="easyui-numberbox" name="age" data-options="min:0,max:100"/></dd>
		<dt>所在单位：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="danwei" data-options="required:true"/></dd>
		<dt>授课专业：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="zhuanye" data-options="required:true"/></dd>
		<dt>联系电话：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="phone" data-options="validType:'mobile'"/></dd>
		<dt>Email：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="email" data-options="validType:'email'"/></dd>
		<security:authorize ifAllGranted="ROLE_ADMIN">
		<dt>角色：</dt> 
		<dd>
			<select type="text" name="role" style="width: 155px;">
				<option value="1">管理员</option>
				<option value="2">普通用户</option>
			</select>
		</dd>
	</dl>
</form>
	
</div>

<div id="detail_buttons">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="save()">保存</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#user_detail').dialog('close');">取消</a>
</div>

<script type="text/javascript">
	
function query() {
	var params = {};
	
	params.username = $('#username').val();
	params.fullName = $('#fullName').val();
	
	$('#user_grid').datagrid('options').queryParams = params;
	$('#user_grid').datagrid('reload');
}

function showAddDialog() {
	$("#user_detail")
		.dialog('setTitle', '新增用户')
		.dialog('open');
	$('input[name="username"]','#user_detail_form').removeAttr('disabled');
	$('#user_detail_form').form('clear');
}

function showModifyDialog(rowData) {
	if (!rowData) {
		var row = $('#user_grid').datagrid('getSelected');
		if (row) {
			rowData = row;
		} else {
			$.alert('请选择要修改的记录！');
		}
	}

	if(rowData){
		$("#user_detail")
			.dialog('setTitle', '修改用户')
			.dialog('open');
		$('#user_detail_form').form('load','${path}/user/admin/detail?userId=' + rowData.userId);
		$('input[name="username"]','#user_detail_form').attr('disabled','disabled');
	}
}

/**
 * 
 */
function save() {
	var url = '${path}/user/save'
	
	$('#user_detail_form').form('submit', {
		url : url,
		onSubmit : function() {
			return $('#user_detail_form').form('validate');
		},
		success : function(data) {
			$('#user_detail').dialog('close');
			$('#user_grid').datagrid('reload');
			var rs = $.evalJSON(data);
			$.message(rs.msg);
		}
	});
}

/**
 * 
 */
function deleteRow() {
	var row = $('#user_grid').datagrid('getSelected');
	var index = $('#user_grid').datagrid('getRowIndex', row);
	if (row) {
		$.confirm('请确认是否删除选择的记录?', function() {
			var url = '${path}/user/admin/delete';
			$.post(url, {
				userId : row.userId
			},
			function(rs){
				alert(rs);
				if(rs.success){
					$.message(rs.msg || '删除用户成功！');
					$('#user_grid').datagrid('reload');
				}else{
					$.message(rs.msg || '删除用户失败！');
				}
			},'json');
		});
	} else {
		$.alert('请选择要删除的记录！');
	}
}
</script>