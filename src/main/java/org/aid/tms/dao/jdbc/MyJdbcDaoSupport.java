package org.aid.tms.dao.jdbc;

import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class MyJdbcDaoSupport extends JdbcDaoSupport{
	private SimpleJdbcCall simpleJdbcCall;
	
	@Override
	protected void initTemplateConfig() {
		super.initTemplateConfig();
		this.simpleJdbcCall = new SimpleJdbcCall(getJdbcTemplate());
	}

	/**
	 * Return a SimpleJdbcCall wrapping the configured JdbcTemplate.
	 */
	public SimpleJdbcCall getSimpleJdbcCall() {
	  return this.simpleJdbcCall;
	}
}
