package org.aid.commons;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

public class LoginUserManger {
	private LoginUserManger() {
	}

	public static User getLoginUser() {
		SecurityContext context = SecurityContextHolder.getContext();
		if (context == null) {
			return null;
		}
		
		Authentication authentication = context.getAuthentication();
		if (authentication == null) {
			return null;
		}
		
		return (User) authentication.getPrincipal();
	}

	public static String getLoginUsername() {
		User u = getLoginUser();
		if (u != null) {
			return ((User) u).getUsername();
		}
		return null;
	}

	public static Collection<GrantedAuthority> getAuthorities() {
		User u = getLoginUser();
		if (u != null) {
			return u.getAuthorities();
		}

		return null;
	}
	
	public static boolean hasAllGranted(String ... roleFlag){
		
		return false;
	}
	
	public static boolean hasAnyGranted(String ... roleFlag){
		
		return false;
	}
	
	public static boolean hasNoneGranted(String ... roleFlag){
		
		return false;
	}
}
