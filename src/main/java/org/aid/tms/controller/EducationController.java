package org.aid.tms.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aid.commons.IPagination;
import org.aid.commons.LoginUserManger;
import org.aid.tms.model.TBook;
import org.aid.tms.model.TEducation;
import org.aid.tms.model.TUser;
import org.aid.tms.service.BaseService;
import org.aid.tms.service.EducationService;
import org.aid.tms.service.ServiceException;
import org.aid.tms.service.UserService;
import org.aid.tms.vo.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/education")
public class EducationController extends BaseController {
	@Autowired
	private EducationService educationService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/view")
	public String goEducationPage(SecurityContextHolderAwareRequestWrapper request,Model model){
		String username = null;
		if(!request.isUserInRole("ROLE_ADMIN")){
			username = LoginUserManger.getLoginUsername();
		}
		List<TUser> users = userService.getUsersByUsername(username);
		model.addAttribute("users", users);
		return "education";
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> getEducationList(Integer page,Integer rows,String queryParams){
		Map<String,Object> results = new HashMap<String, Object>();
		QueryParam[] params = null;
		if(StringUtils.isNotEmpty(queryParams)){
			try {
				params = new ObjectMapper().readValue(queryParams, QueryParam[].class);
			} catch (Throwable e) {
				System.out.println(e.getMessage());
			} 
		}
		IPagination<?> pagination = educationService.getListByPage(page,rows,params==null?null:Arrays.asList(params));
		
		results.put("total", pagination.getTotalCount());
		results.put("rows", pagination.getResults());
		return results;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public Map<String,Object> saveEducation(TEducation education){
		Map<String,Object> results = new HashMap<String, Object>();
		if(education != null){
			if(education.getEducationId() == null){
				education.setCreateTime(new Timestamp(new Date().getTime()));
				education.setCreateUsername(LoginUserManger.getLoginUsername());
			}else{
				education.setLastUpdateTime(new Timestamp(new Date().getTime()));
				education.setLastUpdateUsername(LoginUserManger.getLoginUsername());
			}
		}
		
		try{
			educationService.save(education);
			results.put("success", true);
			results.put("msg","保存教育背景成功");
		}catch(ServiceException e){
			results.put("msg","保存教育背景失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	public Object getEducation(Integer educationId){
		return educationService.getById(educationId);
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String,Object> deleteEducationById(Integer educationId){
		
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			educationService.deleteById(educationId);
			results.put("success", true);
			results.put("msg","删除教育背景成功");
		}catch(ServiceException e){
			results.put("msg","删除教育背景失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/export/excel")
	public void exportEducationList(HttpServletRequest request,HttpServletResponse response,String queryParams) throws IOException{
		exportExcel(request, response, queryParams, new BuildExcelCallback() {
			
			@Override
			public BaseService getService() {
				return educationService;
			}
			
			@Override
			public String getFileName() {
				return "教育背景";
			}
			
			@Override
			public String[] getColumnHeads() {
				return new String[]{"姓名","开始时间","结束时间","毕业院校","专业","获得学位","年度"};
			}

			@Override
			public void fillRowData(HSSFRow row, Object d,CellStyle baseStyle) {
				TEducation e = (TEducation)d;
				setCellValue(row,0,e.getUser().getFullName(),baseStyle);
				setCellValue(row,1,e.getBeginTime());
				setCellValue(row,2,e.getEndTime());
				setCellValue(row,3,e.getCollege(),baseStyle);
				setCellValue(row,4,e.getMajor(),baseStyle);
				setCellValue(row,5,e.getDegree(),baseStyle);
				setCellValue(row,6,e.getYear(),baseStyle);
			}
		});
	}
}
