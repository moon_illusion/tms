package org.aid.tms.dao.hibernate;

import org.aid.tms.model.TEducation;
import org.springframework.stereotype.Repository;

@Repository
public class EducationDao extends BaseDao<TEducation, Integer> {

	@Override
	protected Class<TEducation> getEntityClass() {
		return TEducation.class;
	}

}
