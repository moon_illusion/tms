package org.aid.tms.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aid.commons.IPagination;
import org.aid.commons.LoginUserManger;
import org.aid.tms.model.TSubject;
import org.aid.tms.model.TUser;
import org.aid.tms.service.BaseService;
import org.aid.tms.service.ServiceException;
import org.aid.tms.service.UserService;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController{
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/view")
	public String view(){
		return "user";
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public Map<String,Object> getUserList(Integer page,Integer rows,String username,String fullName){
		
		Map<String,Object> results = new HashMap<String, Object>();
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("username", username);
		params.put("fullName", fullName);
		IPagination<?> pagination = userService.getUsersByPage(page,rows,params);
		
		results.put("total", pagination.getTotalCount());
		results.put("rows", pagination.getResults());
		return results;
	}
	
	@RequestMapping("/admin/save")
	@ResponseBody
	public Map<String,Object> saveUser(TUser user){
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			userService.saveUser(user);
			results.put("success", true);
			results.put("msg","保存用户成功");
		}catch(ServiceException e){
			results.put("msg","保存用户失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/admin/detail")
	@ResponseBody
	public TUser getUser(Integer userId){
		return userService.getUserById(userId);
	}
	
	@RequestMapping("/admin/delete")
	@ResponseBody
	public Map<String,Object> deleteUser(Integer userId){
		
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			userService.deleteUserById(userId);
			results.put("success", true);
			results.put("msg","删除用户成功");
		}catch(ServiceException e){
			results.put("msg","删除用户失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/self")
	@ResponseBody
	public TUser getSelfDetail(){
		String username = LoginUserManger.getLoginUsername();
		return userService.getUserByUsername(username);
	}
	
	@RequestMapping("/modify")
	@ResponseBody
	public Map<String,Object> modifySelf(TUser user){
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			userService.modfiySelf(user,LoginUserManger.getLoginUsername());
			results.put("success", true);
			results.put("msg","修改用户成功");
		}catch(ServiceException e){
			results.put("msg","修改用户失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/admin/setPass")
	@ResponseBody
	public Map<String,Object> setPassword(String password,String verifyPass){
		
		return null;
	}
	
	@RequestMapping("/export/excel")
	public void exportSubjectList(HttpServletRequest request,HttpServletResponse response,String queryParams) throws IOException{
		exportExcel(request, response, queryParams, new BuildExcelCallback() {
			
			@Override
			public BaseService getService() {
				return userService;
			}
			
			@Override
			public String getFileName() {
				return "教师列表";
			}
			
			@Override
			public String[] getColumnHeads() {
				return new String[]{"姓名","所在单位","所属专业","性别","年龄","手机号","Email","角色"};
			}

			@Override
			public void fillRowData(HSSFRow row, Object d,CellStyle baseStyle) {
				TUser u = (TUser)d;
				setCellValue(row,0,u.getFullName(),baseStyle);
				setCellValue(row,1,u.getDanwei(),baseStyle);
				setCellValue(row,2,u.getZhuanye(),baseStyle);
				setCellValue(row,3,"1".equals(u.getSex())?"男":("2".equals(u.getSex())?"女":"其他"),baseStyle);
				setCellValue(row,4,u.getAge(),baseStyle);
				setCellValue(row,5,u.getPhone(),baseStyle);
				setCellValue(row,6,u.getEmail(),baseStyle);
				setCellValue(row,7,u.getRole(),baseStyle);
			}
		});
	}
}
