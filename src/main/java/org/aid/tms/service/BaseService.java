package org.aid.tms.service;


import java.io.Serializable;
import java.util.List;

import org.aid.commons.IPagination;
import org.aid.tms.dao.hibernate.BaseDao;
import org.aid.tms.vo.QueryParam;

public abstract class BaseService {
	public abstract BaseDao<Object, Serializable> getBaseDao();
	
	public abstract String getObjName();
	
	public IPagination<?> getListByPage(Integer page, Integer rows,
			List<QueryParam> queryParams) {
		return getBaseDao().getPageList(page, rows, queryParams);
	}
	
	public List<?> getList(List<QueryParam> queryParams) {
		return getBaseDao().getList(queryParams);
	}
	
	public void save(Object o) throws ServiceException{
		if(o == null){
			throw new ServiceException("要保存的" + getObjName() + "不能为空");
		}
		try{
			getBaseDao().saveOrUpdate(o);
		}catch(Throwable e){
			throw new ServiceException(e.getMessage(),e);
		}
	}
	
	public Object getById(Serializable id){
		return getBaseDao().get(id);
	}
	
	public void deleteById(Serializable id){
		if(id == null){
			throw new ServiceException("必须提供要删除" + getObjName() + "的Id");
		}
		
		Object o = getBaseDao().get(id);
		if(o == null){
			throw new ServiceException("要删除的" + getObjName() + "不能为空");
		}
		
		try{
			getBaseDao().delete(o);
		}catch(Throwable e){
			throw new ServiceException(e.getMessage(),e);
		}
	}
}
