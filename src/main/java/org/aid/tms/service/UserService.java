package org.aid.tms.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.aid.commons.IPagination;
import org.aid.tms.dao.hibernate.BaseDao;
import org.aid.tms.dao.hibernate.UserDao;
import org.aid.tms.model.TUser;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService extends BaseService{
	
	@Autowired
	private UserDao userDao;

	public IPagination<?> getUsersByPage(Integer page, Integer rows,
			Map<String, Object> params) {
		
		return userDao.findUsersByPage(page,rows,params);
	}

	public TUser getUserById(Integer userId) {
		return userDao.get(userId);
	}

	public void updateUser(TUser user) throws ServiceException{
		if(user != null){
			TUser u = userDao.get(user.getUserId());
			if(u == null){
				throw new ServiceException("要修改的用户不存在");
			}
			copyUserProperties(user,u);
			try{
				userDao.update(u);
			}catch(Throwable e){
				throw new ServiceException(e.getMessage(),e);
			}
		}
	}
	
	public void modfiySelf(TUser user,String username){
		if(user != null){
			TUser u = getUserByUsername(username);
			if(u == null){
				throw new ServiceException("要修改的用户不存在");
			}
			copyUserProperties(user,u);
			try{
				userDao.update(u);
			}catch(Throwable e){
				throw new ServiceException(e.getMessage(),e);
			}
		}
	}
	
	private void copyUserProperties(TUser user,TUser u){
		if(user == null && u == null){
			return;
		}
		u.setAge(user.getAge());
		u.setDanwei(user.getDanwei());
		u.setEmail(user.getEmail());
		u.setFullName(user.getFullName());
		u.setPassword(user.getPassword());
		u.setPhone(user.getPhone());
		u.setRole(u.getRole());
		u.setSex(user.getSex());
		u.setZhuanye(user.getZhuanye());
	}
	
	public void saveUser(TUser user) throws ServiceException{
		if(user != null){
			if(user.getUserId() == null){
				addUser(user);
			}else{
				updateUser(user);
			}
		}
	}
	
	public void addUser(TUser user) throws ServiceException{
		if(user != null){
			String username = user.getUsername();
			if(StringUtils.isEmpty(username)){
				throw new ServiceException("用户名不能为空");
			}
			
			int num = userDao.countByProperty("username", username);
			if(num > 0){
				throw new ServiceException("用户名已被使用");
			}
			user.setCreateTime(new Date());
			
			try{
				userDao.save(user);
			}catch(Throwable e){
				throw new ServiceException(e.getMessage(),e);
			}
		}
	}

	public void deleteUserById(Integer userId) throws ServiceException {
		if(userId == null){
			throw new ServiceException("请选择要删除的用户");
		}
		
		TUser u = userDao.get(userId);
		if(u == null){
			throw new ServiceException("要删除的用户不存在或已被删除");
		}
		try{
			userDao.delete(u);
		}catch(Throwable e){
			throw new ServiceException(e.getMessage(),e);
		}
	}

	public TUser getUserByUsername(String username) {
		return userDao.findUniqueByProperty("username", username);
	}

	public List<TUser> getUsersByUsername(String username) {
		if(StringUtils.isNotEmpty(username)){
			return userDao.findByProperty("username", username);
		}
		return userDao.findAll();
	}

	@Override
	public BaseDao getBaseDao() {
		return userDao;
	}

	@Override
	public String getObjName() {
		return "用户";
	}
}
