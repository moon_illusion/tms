package org.aid.tms.constants;

public interface Relation {
	String EQUAL = "=";
	String LESS_THAN = "<";
	String GREATE_THAN = ">";
	String LESS_EQUAL = "<=";
	String GREATE_EQUAL = ">=";
	String LIKE = "like";
}
