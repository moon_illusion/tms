package org.aid.tms.dao.hibernate;

import org.aid.tms.model.Topic;
import org.springframework.stereotype.Repository;

@Repository
public class TopicDao extends BaseDao<Topic, Integer> {

	@Override
	protected Class<Topic> getEntityClass() {
		return Topic.class;
	}
}
