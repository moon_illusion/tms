package org.aid.tms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.aid.commons.DateTimeSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 工作简历
 * @author lee
 *
 */
@Entity
@Table(name="t_resume")
public class TResume implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer resumeId;
	
	private Date beginTime;
	private Date endTime;
	
	private String danwei;
	
	private String post;
	
	//职称
	private String title;
	
	//曾任职情况
	private String postHistory;
	
	private String year;
	
	private Date createTime;
	private String createUsername;
	
	private Date lastUpdateTime;
	private String lastUpdateUsername;
	
	private TUser user;

	@Column
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@JsonSerialize(using=DateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name="create_username")
	public String getCreateUsername() {
		return createUsername;
	}

	public void setCreateUsername(String createUsername) {
		this.createUsername = createUsername;
	}

	@JsonSerialize(using=DateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_update_time")
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	@Column(name="last_update_username")
	public String getLastUpdateUsername() {
		return lastUpdateUsername;
	}

	public void setLastUpdateUsername(String lastUpdateUsername) {
		this.lastUpdateUsername = lastUpdateUsername;
	}

	@ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH }, optional = true)  
    @JoinColumn(name="user_id")
	public TUser getUser() {
		return user;
	}

	public void setUser(TUser user) {
		this.user = user;
	}
	
	@Id
	@Column(name="resume_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getResumeId() {
		return resumeId;
	}

	public void setResumeId(Integer resumeId) {
		this.resumeId = resumeId;
	}

	@Column(name="begin_time")
	@Temporal(TemporalType.DATE)
	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	@Column(name="end_time")
	@Temporal(TemporalType.DATE)
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Column
	public String getDanwei() {
		return danwei;
	}

	public void setDanwei(String danwei) {
		this.danwei = danwei;
	}

	@Column
	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	@Column
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="post_history")
	public String getPostHistory() {
		return postHistory;
	}

	public void setPostHistory(String postHistory) {
		this.postHistory = postHistory;
	}
}
