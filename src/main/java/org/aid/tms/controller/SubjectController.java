package org.aid.tms.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aid.commons.IPagination;
import org.aid.commons.LoginUserManger;
import org.aid.tms.model.TResume;
import org.aid.tms.model.TSubject;
import org.aid.tms.model.TUser;
import org.aid.tms.service.BaseService;
import org.aid.tms.service.ServiceException;
import org.aid.tms.service.SubjectService;
import org.aid.tms.service.UserService;
import org.aid.tms.vo.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/subject")
public class SubjectController extends BaseController {
	@Autowired
	private SubjectService subjectService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/view")
	public String goSubjectPage(SecurityContextHolderAwareRequestWrapper request,Model model){
		String username = null;
		if(!request.isUserInRole("ROLE_ADMIN")){
			username = LoginUserManger.getLoginUsername();
		}
		List<TUser> users = userService.getUsersByUsername(username);
		model.addAttribute("users", users);
		return "subject";
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> getSubjectList(Integer page,Integer rows,String queryParams){
		Map<String,Object> results = new HashMap<String, Object>();
		QueryParam[] params = null;
		if(StringUtils.isNotEmpty(queryParams)){
			try {
				params = new ObjectMapper().readValue(queryParams, QueryParam[].class);
			} catch (Throwable e) {
				System.out.println(e.getMessage());
			} 
		}
		IPagination<?> pagination = subjectService.getListByPage(page,rows,params==null?null:Arrays.asList(params));
		
		results.put("total", pagination.getTotalCount());
		results.put("rows", pagination.getResults());
		return results;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public Map<String,Object> saveSubject(TSubject subject){
		Map<String,Object> results = new HashMap<String, Object>();
		if(subject != null){
			if(subject.getSubjectId() == null){
				subject.setCreateTime(new Timestamp(new Date().getTime()));
				subject.setCreateUsername(LoginUserManger.getLoginUsername());
			}else{
				subject.setLastUpdateTime(new Timestamp(new Date().getTime()));
				subject.setLastUpdateUsername(LoginUserManger.getLoginUsername());
			}
		}
		
		try{
			subjectService.save(subject);
			results.put("success", true);
			results.put("msg","保存科目成功");
		}catch(ServiceException e){
			results.put("msg","保存科目失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	public Object getSubject(Integer subjectId){
		return subjectService.getById(subjectId);
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String,Object> deleteSubjectById(Integer subjectId){
		
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			subjectService.deleteById(subjectId);
			results.put("success", true);
			results.put("msg","删除科目成功");
		}catch(ServiceException e){
			results.put("msg","删除科目失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/export/excel")
	public void exportSubjectList(HttpServletRequest request,HttpServletResponse response,String queryParams) throws IOException{
		exportExcel(request, response, queryParams, new BuildExcelCallback() {
			
			@Override
			public BaseService getService() {
				return subjectService;
			}
			
			@Override
			public String getFileName() {
				return "教学列表";
			}
			
			@Override
			public String[] getColumnHeads() {
				return new String[]{"姓名","开始时间","结束时间","课程(实验)名称","是否双语授课","授课专业","学时(周数)","人数","年度"};
			}

			@Override
			public void fillRowData(HSSFRow row, Object d,CellStyle baseStyle) {
				TSubject s = (TSubject)d;
				setCellValue(row,0,s.getUser().getFullName(),baseStyle);
				setCellValue(row,1,s.getBeginTime());
				setCellValue(row,2,s.getEndTime());
				setCellValue(row,3,s.getSubjectName(),baseStyle);
				setCellValue(row,4,s.getBilingual()==1?"是":"否",baseStyle);
				setCellValue(row,5,s.getMajor(),baseStyle);
				setCellValue(row,6,s.getClassHour()+(s.getClassUnit()==1?"学时":"周"),baseStyle);
				setCellValue(row,7,s.getPersonNum(),baseStyle);
				setCellValue(row,8,s.getYear(),baseStyle);
			}
		});
	}
}
