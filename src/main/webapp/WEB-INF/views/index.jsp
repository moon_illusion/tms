<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<c:set var="path" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="pragma" content="no-cache"/> 
	<meta http-equiv="cache-control" content="no-cache"/> 
	<meta http-equiv="expires" content="0"/>
	<link rel="stylesheet" type="text/css" href="${path}/resources/framework/easyui/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="${path}/resources/framework/easyui/themes/icon.css"/>
	
	<script type="text/javascript" src="${path}/resources/framework/easyui/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="${path}/resources/framework/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${path}/resources/framework/easyui/locale/easyui-lang-zh_CN.js"></script>
	
	<link rel="stylesheet" type="text/css" href="${path}/resources/css/main.css"/>
	<script type="text/javascript" src="${path}/resources/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="${path}/resources/js/jquery.extend.js"></script>
	<style type="text/css">
		.easyui-tree li a {text-decoration: none;}
	</style>
</head>
<body class="easyui-layout">
	<div region="north" border="false" split="true" id="head">
			<div style="height: 30px;"></div>
			<div style="padding-right:50px;text-align:right;"> 欢迎你,
				<security:authentication property="principal.username" /> !
			 	<a href="${path}/<c:url value='j_spring_security_logout'/>">注销 </a>
			 </div>
	</div>
	<div region="west" split="true" title="菜单" id="left">
		<div class="easyui-accordion" fit="true" border="false" id="nav">
			<div title="基本信息">
				<ul class="easyui-tree">
					 <li><a href="javascript:void(0);" onclick="openTab('教育背景','education/view')"><span>教育背景</span></a></li>
					 <li><a href="javascript:void(0);" onclick="openTab('工作简历','resume/view')"><span>工作简历</span></a></li>
				</ul>
			</div>
			<div title="学科建设">
				<ul class="easyui-tree">
					 <li><a href="javascript:void(0);" onclick="openTab('教学列表','subject/view')"><span>教学列表</span></a></li>
				</ul>
			</div>
			<div title="教学科研">
				<ul class="easyui-tree">
					 <li><a href="javascript:void(0);" onclick="openTab('在研课题','topic/view')"><span>在研课题</span></a></li>
					 <li><a href="javascript:void(0);" onclick="openTab('发表论文','treatise/view')"><span>发表论文</span></a></li>
					 <li><a href="javascript:void(0);" onclick="openTab('发表论著','book/view')"><span>发表论著</span></a></li>
					 <li><a href="javascript:void(0);" onclick="openTab('获奖情况','prize/view')"><span>获奖情况</span></a></li>
				</ul>
			</div>
			<div title="师资队伍">
				<ul class="easyui-tree">
					 <li><a href="javascript:void(0);" onclick="openTab('教师列表','user/view')"><span>教师列表</span></a></li>
				</ul>
			</div>
			<%--
			<div title="查询打印">
				<ul class="easyui-tree">
					 <li><a href="javascript:void(0);" onclick="openTab('查询打印','export/view')"><span>查询打印</span></a></li>
				</ul>
			</div>
			 --%>
		</div>
	</div>
	<div region="center" id="main">
		<div id="tabs" class="easyui-tabs" data-options="fit:true,boder:false">
			<div title="欢迎"></div>
		</div>
	</div>
	<div region="south" split="true" id="foot">
		<div class="footer">Copyright ,Ltd All rights reserved.</div>
	</div>
	<script type="text/javascript">
		var tb = $('#tabs');
		function openTab(title,url){
			if(title && title != '' && url && url != ''){
				var exists = tb.tabs('exists',title);
				if(exists){
					tb.tabs('select',title)
				}else{
					tb.tabs('add',{
						title:title,
						href:'${path}/'+url,
						closable:true,
						tools:[{
							iconCls:'icon-mini-refresh',
							handler:function(e){
								refresh(title);
								e.stopPropagation();  
							}
						}]
					});
					tb.tabs('addEventParam');
				}
			}
		}
		
		function refresh(title){
			if(title && title != ''){
				var p = tb.tabs('getTab',title);
				if(p){
					p.panel('refresh');
				}
			}
		}
	</script>
</body>
</html>
