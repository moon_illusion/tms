package org.aid.commons;

import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

public class CustomerH2DataSource extends SimpleDriverDataSource{

	@Override
	public void setUrl(String url){
		if(StringUtils.isNotEmpty(url)){
			String[] parts = url.split(":");
			if(parts != null && parts.length > 3){
				if("h2".equals(parts[1]) && "res".equals(parts[2])){
					URL u = getClass().getResource("/");
					if(u != null){
						String prePath = u.getPath();
						if(StringUtils.isNotEmpty(prePath)){
							parts[3] = prePath + parts[3];
						}
					}
					
					parts[2] = "file";
					url = StringUtils.join(parts, ":");
				}
			}
			super.setUrl(url);
		}
	}
}
