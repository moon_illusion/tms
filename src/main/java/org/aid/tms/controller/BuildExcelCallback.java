package org.aid.tms.controller;

import org.aid.tms.service.BaseService;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;

public interface BuildExcelCallback {
	public String getFileName();
	
	public String[] getColumnHeads();
	
	public BaseService getService();
	
	public void fillRowData(HSSFRow row,Object d,CellStyle baseStyle);
	
}
