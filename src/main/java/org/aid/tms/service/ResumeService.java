package org.aid.tms.service;

import org.aid.tms.dao.hibernate.BaseDao;
import org.aid.tms.dao.hibernate.ResumeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResumeService extends BaseService {

	@Autowired
	private ResumeDao resumeDao;
	
	@Override
	public BaseDao getBaseDao() {
		return resumeDao;
	}

	@Override
	public String getObjName() {
		return "简历";
	}
}
