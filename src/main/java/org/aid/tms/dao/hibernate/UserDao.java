package org.aid.tms.dao.hibernate;

import java.util.Map;
import java.util.Map.Entry;

import org.aid.commons.IPagination;
import org.aid.tms.model.TUser;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao extends BaseDao<TUser, Integer> {

	@Override
	protected Class<TUser> getEntityClass() {
		return TUser.class;
	}

	/**
	 * @param page
	 * @param rows
	 * @param params
	 * @return
	 */
	public IPagination<TUser> findUsersByPage(Integer page, Integer rows,
			Map<String, Object> params) {
		DetachedCriteria criteria = DetachedCriteria.forClass(getEntityClass());
		if(params != null){
			for(Entry<String, Object> entry : params.entrySet()){
				String k = entry.getKey();
				Object v = entry.getValue();
				if( v != null){
					if(v instanceof CharSequence){
						if(StringUtils.isNotEmpty(v.toString())){
							criteria.add(Restrictions.like(k, v.toString()));
						}
					}else{
						criteria.add(Restrictions.eq(k, v));
					}
				}
			}
		}
		
		return findByCriteria(criteria, page, rows);
	}
}
