package org.aid.tms.dao.hibernate;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.aid.commons.Finder;
import org.aid.commons.IPagination;
import org.aid.commons.Pagination;
import org.apache.commons.lang.SerializationUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.util.Assert;

public abstract class BaseHibernateDao<T, TID extends Serializable> extends HibernateDaoSupport{
	/**
	 * 日志记录器
	 */
	protected Logger log = LoggerFactory.getLogger(getClass());
	
	
	/**
	 * 保存对象
	 * @param obj
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public TID save(T obj) {
		return (TID)getHibernateTemplate().save(obj);
	}

	/**
	 * 删除对象
	 * @param obj
	 */
	public void delete(T obj) {
		delete(obj,false);
	}
	
	/**
	 * 删除对象
	 * @param obj
	 * @param lock 是否以LockMode.UPGRADE_NOWAIT模式加锁
	 */
	public void delete(T obj,boolean lock) {
		if(lock){
			getHibernateTemplate().delete(obj,LockMode.UPGRADE_NOWAIT);
		}else{
			getHibernateTemplate().delete(obj);
		}
	}
	
	/**
	 * 删除集合中所有对象
	 * @param collection
	 */
	public void deleteAll(Collection<T> collection) {
		if(collection != null && collection.size() > 0){
			getHibernateTemplate().deleteAll(collection);
		}
	}

	
	/**
	 * 通过id删除对象
	 * @param id
	 */
	public void deleteById(TID id) {
		Object obj = getHibernateTemplate().get(getEntityClass(), id);
		
		if(obj != null){
			getHibernateTemplate().delete(obj);
		}
	}

	/**
	 * 更新对象
	 * @param obj
	 */
	public void update(T obj) {
		update(obj,false);
	}
	
	/**
	 * 更新对象
	 * @param obj
	 * @param lock 是否以LockMode.UPGRADE_NOWAIT方式加锁
	 */
	public void update(T obj,boolean lock) {
		if(lock){
			getHibernateTemplate().update(obj,LockMode.UPGRADE_NOWAIT);
		}else{
			getHibernateTemplate().update(obj);
		}
	}

	/**
	 * 更新或保存对象
	 * @param obj
	 */
	public void saveOrUpdate(T obj) {
		getHibernateTemplate().saveOrUpdate(obj);
	}
	
	/**
	 * 获取实体类型
	 * @return
	 */
	abstract protected Class<T> getEntityClass();
	
	/**
	 * 获取对象
	 * @param id
	 * @return
	 */
	public T get(TID id) {
		return get(id, false);
	}
	
	/**
	 * 获取对象
	 * @param id
	 * @return
	 */
	public T get(TID id,boolean lock) {
		T entity;
		if (lock) {
			entity = (T) getHibernateTemplate().get(getEntityClass(), id,
					LockMode.UPGRADE_NOWAIT);
		} else {
			entity = (T) getHibernateTemplate().get(getEntityClass(), id);
		}
		return entity;
	}

	/**
	 * 获取所有对象
	 * @return
	 */
	public List<T> findAll() {
		return getHibernateTemplate().loadAll(getEntityClass());
	}
	
	/**
	 * 查询对象
	 * @param hql
	 * @param values
	 * @return
	 */
	protected List<?> find(String hql, Object... values) {
		return getHibernateTemplate().find(hql, values);
	}

	
	/**
	 * 查询唯一对象
	 * @param hql
	 * @param values
	 * @return
	 */
	protected Object findUnique(final String hql,final Object... values) {
		return getHibernateTemplate().execute(new HibernateCallback<Object>() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException,
					SQLException {
				Query query = session.createQuery(hql);
				
				if(values != null){
					int size = values.length;
					for(int i=0;i<size;i++){
						query.setParameter(i, values[i]);
					}
				}
				return query.uniqueResult();
			}
		});
	}
	
	
	/**
	 * 分页查询
	 * @param hql
	 * @param pageNo
	 * @param pageSize
	 * @param values
	 * @return
	 */
	protected IPagination<?> findByPage(String hql, int pageNo, int pageSize,Object... values) {
		Finder finder = Finder.create(hql,values);
		return findByPage(finder, pageNo, pageSize);
	}
	
	/**
	 * 分页查询
	 * @param finder
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	protected IPagination<?> findByPage(final Finder finder, int pageNo, int pageSize) {
		
		int totalCount = countQueryResult(finder);
		Pagination<Object> pagination = new Pagination<Object>(pageNo, pageSize, totalCount);
		if (totalCount < 1) {
			pagination.setResults(new ArrayList<Object>());
			return pagination;
		}
		
		final int first = pagination.getCurrPageFirst();
		final int max = pagination.getPageSize();
		
		List<Object> list = (List<Object>)getHibernateTemplate().execute(new HibernateCallback<List<Object>>() {

			@SuppressWarnings("rawtypes")
			@Override
			public List doInHibernate(Session session) throws HibernateException,
					SQLException {
				Query query = session.createQuery(finder.getOrigHql());
				finder.setParamsToQuery(query);
				query.setFirstResult(first);
				query.setMaxResults(max);
				if (finder.isCacheable()) {
					query.setCacheable(true);
				}
				return query.list();
			}
		});
		
		pagination.setResults(list);
		return pagination;
	}
	
	/**
	 * 按Criterion查询列表数据.
	 * @param criterion
	 * 数量可变的Criterion.
	 */
	protected List<T> findByCriteria(Criterion... criterions) {
		return findByCriteria(createCriteria(criterions));
	}
	
	/**
	 * 查询对象
	 * @param criteria
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(DetachedCriteria criteria){
		return getHibernateTemplate().findByCriteria(criteria);
	}
	
	
	/**
	 * 分页查询
	 * @param pageNo
	 * @param pageSize
	 * @param criterions
	 * @return
	 */
	protected IPagination<T> findByCriteria(int pageNo, int pageSize, Criterion... criterions) {
		return findByCriteria(createCriteria(criterions),pageNo,pageSize);
	}
	
	/**
	 * 分页查询
	 * @param criteria
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	protected IPagination<T> findByCriteria(DetachedCriteria criteria, int pageNo, int pageSize){
		
		DetachedCriteria clone = (DetachedCriteria) SerializationUtils.clone(criteria);
		
		int totalCount = countQueryResult(clone);
		
		Pagination<T> pagination = new Pagination<T>(pageNo, pageSize, totalCount);
		if (totalCount < 1) {
			pagination.setResults(new ArrayList<T>());
			return pagination;
		}
		
		@SuppressWarnings("unchecked")
		List<T> results = getHibernateTemplate().findByCriteria(criteria, pagination.getCurrPageFirst(), pagination.getPageSize());
		pagination.setResults(results);
		
		return pagination;
	}
	
	/**
	 * @param property
	 * @param value
	 * @return
	 */
	public List<T> findByProperty(String property, Object value) {
		Assert.hasText(property);
		return findByCriteria(createCriteria(Restrictions.eq(property, value)));
	}

	/**
	 * 按属性查找唯一对象
	 */
	public T findUniqueByProperty(String property, Object value) {
		Assert.hasText(property);
		Assert.notNull(value);
		List<T> list = findByCriteria(createCriteria(Restrictions.eq(property, value)));
		
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		
		return null;
	}

	/**
	 * 按属性统计记录数
	 * 
	 * @param property
	 * @param value
	 * @return
	 */
	public int countByProperty(String property, Object value) {
		Assert.hasText(property);
		Assert.notNull(value);
		
		return countQueryResult(createCriteria(Restrictions.eq(property, value)));
	}
	
	
	/**
	 * @param finder
	 * @return
	 */
	protected int countQueryResult(final Finder finder) {
		
		Number num = getHibernateTemplate().execute(new HibernateCallback<Number>() {

			@Override
			public Number doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				Query query = getSession().createQuery(finder.getRowCountHql());
				finder.setParamsToQuery(query);
				if (finder.isCacheable()) {
					query.setCacheable(true);
				}
				return (Number) query.iterate().next();
			}
		});
		
		return num == null?0:num.intValue();
	}
	
	/**
	 * @param criteria
	 * @return
	 */
	protected int countQueryResult(final DetachedCriteria detachedCriteria) {
		Assert.notNull(detachedCriteria);
		
		detachedCriteria.setProjection(Projections.rowCount());
	
		Number num = getHibernateTemplate().execute(new HibernateCallback<Number>() {
	
			@Override
			public Number doInHibernate(Session session)
					throws HibernateException, SQLException {
				 Criteria criteria = detachedCriteria  
	                     .getExecutableCriteria(session);
				return (Number)criteria.uniqueResult();
			}
		
		});
		
		return num == null?0:num.intValue();
	}
	
	/**
	 * 使用Criterion条件创建DetachedCriteria.
	 * @param criterions
	 * @return
	 */
	protected DetachedCriteria createCriteria(Criterion... criterions) {
		DetachedCriteria criteria = DetachedCriteria.forClass(getEntityClass());
		
		for (Criterion c : criterions) {
			criteria.add(c);
		}
		
		return criteria;
	}
	
	
	
	public static final String ROW_COUNT = "select count(*) ";
	public static final String FROM = "from";
	public static final String HQL_FETCH = "fetch";
	public static final String ORDER_BY = "order";
	
	/**
	 * 使用hql进行分页查询
	 * @param pagination
	 * @param hql
	 * @param values
	 * @return
	 */
	protected List<T> findByPage(final Pagination<T> pagination, final String hql,final Object...values) {
		logger.debug("hql:" + hql);
		
		pagination.setTotalCount(countAll(hql));
		
		return getHibernateTemplate().execute(new HibernateCallback<List<T>>() {

			@Override
			public List<T> doInHibernate(Session session) throws HibernateException,
					SQLException {
				Query query = session.createQuery(hql);
				if (values != null) {
					for (int i = 0; i < values.length; i++) {
						logger.debug("parameter " + i + ":" + values[i]);
						query.setParameter(i, values[i]);
					}
				}
				query.setFirstResult(pagination.getCurrPageFirst());
				query.setMaxResults(pagination.getPageSize());

				return query.list();
			}
		
		});
	}
	
	/**
	 * 使用hql查询总数
	 * @param hql
	 * @param values
	 * @return
	 */
	protected int countAll(String hql,final Object...values) {
		final String queryHql = getRowCountHql(hql);
		
		Long val = getHibernateTemplate().execute(new HibernateCallback<Long>() {

			@Override
			public Long doInHibernate(Session session)
					throws HibernateException, SQLException {
				Query query = session.createQuery(queryHql);
				
				if (values != null) {
					for (int i = 0; i < values.length; i++) {
						query.setParameter(i, values[i]);
					}
				}
				
				return (Long)query.uniqueResult();
			}
		});
		
		return val==null?0:val.intValue();
	}
	
	
	/**
	 * 使用sql查询列表
	 * @param sql
	 * @param values
	 * @return
	 */
	protected List findListBySQL(final String sql,final Object...values){
		return findListBySQL(sql, null, values);
	}
	
	/**
	 * @param sql
	 * @param entities
	 * @param values
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected List<Map<String,Object>> findMapList(final String sql,final Object...values){
		return findPaginationBySQL(null, sql, null,true,values);
	}
	
	/**
	 * 使用sql查询列表
	 * @param sql
	 * @param entities
	 * @param values
	 * @return
	 */
	protected List findListBySQL(final String sql,final Map<String,Class<?>> entities,final Object...values){
		return findPaginationBySQL(null, sql, entities,false,values);
	}
	
	
	/**
	 * 使用sql分页查询
	 * @param pagination
	 * @param sql
	 * @param values
	 * @return
	 */
	protected List findPaginationBySQL(final Pagination pagination, final String sql,final Object...values){
		return findPaginationBySQL(pagination, sql, null,values);
	}
	
	/**
	 * @param pagination
	 * @param sql
	 * @param entities
	 * @param values
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected List<Map<String,Object>> findMapPagination(final Pagination pagination, 
			final String sql,final Object...values){
		return findPaginationBySQL(pagination, sql, null,true,values);		
	}
	
	/**
	 * @param pagination
	 * @param sql
	 * @param entities
	 * @param values
	 * @return
	 */
	protected List findPaginationBySQL(final Pagination pagination, final String sql,final Map<String,Class<?>> entities ,final Object...values){
		return findPaginationBySQL(pagination, sql, entities,false,values);
	}
	
	/**
	 * 使用sql分页查询
	 * @param pagination
	 * @param sql
	 * @param entities
	 * @param values
	 * @return
	 */
	protected List findPaginationBySQL(final Pagination pagination, final String sql,
			final Map<String,Class<?>> entities ,final boolean mapModel,final Object...values){
		
		logger.debug("sql:" + sql);
		
		if(pagination != null){
			pagination.setTotalCount(countAllBySQL(sql));
		}
		
		return getHibernateTemplate().execute(new HibernateCallback<List>() {

			@Override
			public List doInHibernate(Session session) throws HibernateException,
					SQLException {
				SQLQuery query = session.createSQLQuery(sql);
				
				if(entities != null){
					Set<Entry<String, Class<?>>> entrySet = entities.entrySet();
					
					for(Entry<String, Class<?>> entry : entrySet){
						if(entry.getKey() != null && entry.getValue() != null){
							query.addEntity(entry.getKey(), entry.getValue());
						}
					}
				}
				
				if (values != null) {
					for (int i = 0; i < values.length; i++) {
						logger.debug("parameter " + i + ":" + values[i]);
						query.setParameter(i, values[i]);
					}
				}
				
				if(pagination != null){
					query.setFirstResult(pagination.getCurrPageFirst());
					query.setMaxResults(pagination.getPageSize());
				}
				
				if(mapModel){
					query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
				}

				return query.list();
			}
		});
	}
	
	/**
	 * 使用sql查询总数
	 * @param sql
	 * @param values
	 * @return
	 */
	protected int countAllBySQL(String sql,final Object...values) {
		final String querySQL = getRowCountSQL(sql);
		
		Number val = getHibernateTemplate().execute(new HibernateCallback<Number>() {

			@Override
			public Number doInHibernate(Session session)
					throws HibernateException, SQLException {
				SQLQuery query = session.createSQLQuery(querySQL);
				if (values != null) {
					for (int i = 0; i < values.length; i++) {
						query.setParameter(i, values[i]);
					}
				}
				return (Number)query.uniqueResult();
			}
		});
		
		
		return val==null?0:val.intValue();
	}
	
	
	protected int sqlUpdate(final String sql,final Object... values){
		return getHibernateTemplate().execute(new HibernateCallback<Integer>() {

			@Override
			public Integer doInHibernate(Session session) throws HibernateException,
					SQLException {
				SQLQuery query = session.createSQLQuery(sql);
				if (values != null) {
					for (int i = 0; i < values.length; i++) {
						query.setParameter(i, values[i]);
					}
				}
				return query.executeUpdate();
			}
		});
	}
	
	/**
	 * 使用查询列表sql生成查询总数sql
	 * @param sql
	 * @return
	 */
	private static String getRowCountSQL(String sql){
		if(sql != null){
			sql = ROW_COUNT + FROM + " (" + clearSql(sql) + ")";
		}
		return sql;
	}
	
	/**
	 * 清除掉排序,提高效率
	 * @param sql
	 * @return
	 */
	private static String clearSql(String sql){
		if(sql == null){
			return null;
		}
		
		String sqlCopy = sql;
		
		int orderIndex = sqlCopy.indexOf(ORDER_BY);
		while(orderIndex > 0){
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append(sqlCopy.substring(0, orderIndex));
			int subIndex = sqlCopy.indexOf(")", orderIndex);
			
			if(subIndex > 0){
				sqlBuilder.append(sqlCopy.substring(subIndex));
			}
			
			sqlCopy = sqlBuilder.toString();
			orderIndex = sqlCopy.indexOf(ORDER_BY);
		}
		
		return sqlCopy;
	}
	
	
	/**
	 * 使用查询列表hql生成查询总数hql
	 * @param hql
	 * @return
	 */
	private String getRowCountHql(String hql) {
		if(hql == null){
			return hql;
		}
		
		int fromIndex = hql.toLowerCase().indexOf(FROM);
		String projectionHql = hql.substring(0, fromIndex);

		hql = hql.substring(fromIndex);
		String rowCountHql = hql.replace(HQL_FETCH, "");

		int index = rowCountHql.indexOf(ORDER_BY);
		if (index > 0) {
			rowCountHql = rowCountHql.substring(0, index);
		}
		return wrapProjection(projectionHql) + rowCountHql;
	}
	
	private String wrapProjection(String projection) {
		if (projection.indexOf("select") == -1) {
			return ROW_COUNT;
		} else {
			return projection.replace("select", "select count(") + ") ";
		}
	}
}
