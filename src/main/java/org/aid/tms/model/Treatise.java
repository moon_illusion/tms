package org.aid.tms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.aid.commons.DateTimeSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * 论文
 * @author lee
 *
 */
@Entity
@Table(name="t_treatise")
public class Treatise implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer treatiseId;
	private String treatiseTitle;
	
	private String publication;
	private String firstAuthor;
	private String cooperator;
	
	//年.卷.期.页码
	private String position;
	
	private String year;
	
	private Date createTime;
	private String createUsername;
	
	private Date lastUpdateTime;
	private String lastUpdateUsername;
	
	private TUser user;

	@Column
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@JsonSerialize(using=DateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name="create_username")
	public String getCreateUsername() {
		return createUsername;
	}

	public void setCreateUsername(String createUsername) {
		this.createUsername = createUsername;
	}

	@JsonSerialize(using=DateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_update_time")
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	@Column(name="last_update_username")
	public String getLastUpdateUsername() {
		return lastUpdateUsername;
	}

	public void setLastUpdateUsername(String lastUpdateUsername) {
		this.lastUpdateUsername = lastUpdateUsername;
	}

	@ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH }, optional = true)  
    @JoinColumn(name="user_id")
	public TUser getUser() {
		return user;
	}

	public void setUser(TUser user) {
		this.user = user;
	}
	
	@Id
	@Column(name="treatise_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getTreatiseId() {
		return treatiseId;
	}

	public void setTreatiseId(Integer treatiseId) {
		this.treatiseId = treatiseId;
	}

	@Column(name="treatise_title")
	public String getTreatiseTitle() {
		return treatiseTitle;
	}

	public void setTreatiseTitle(String treatiseTitle) {
		this.treatiseTitle = treatiseTitle;
	}

	@Column
	public String getPublication() {
		return publication;
	}

	public void setPublication(String publication) {
		this.publication = publication;
	}

	@Column(name="first_author")
	public String getFirstAuthor() {
		return firstAuthor;
	}

	public void setFirstAuthor(String firstAuthor) {
		this.firstAuthor = firstAuthor;
	}

	@Column
	public String getCooperator() {
		return cooperator;
	}

	public void setCooperator(String cooperator) {
		this.cooperator = cooperator;
	}

	@Column
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
}
