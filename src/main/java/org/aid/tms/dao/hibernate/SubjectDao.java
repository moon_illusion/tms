package org.aid.tms.dao.hibernate;

import org.aid.tms.model.TSubject;
import org.springframework.stereotype.Repository;

@Repository
public class SubjectDao extends BaseDao<TSubject, Integer> {

	@Override
	protected Class<TSubject> getEntityClass() {
		return TSubject.class;
	}
}
