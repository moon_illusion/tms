package org.aid.tms.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aid.commons.IPagination;
import org.aid.commons.LoginUserManger;
import org.aid.tms.model.TSubject;
import org.aid.tms.model.TUser;
import org.aid.tms.model.Topic;
import org.aid.tms.service.BaseService;
import org.aid.tms.service.ServiceException;
import org.aid.tms.service.TopicService;
import org.aid.tms.service.UserService;
import org.aid.tms.vo.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/topic")
public class TopicController extends BaseController {
	@Autowired
	private TopicService topicService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/view")
	public String goTopicPage(SecurityContextHolderAwareRequestWrapper request,Model model){
		String username = null;
		if(!request.isUserInRole("ROLE_ADMIN")){
			username = LoginUserManger.getLoginUsername();
		}
		List<TUser> users = userService.getUsersByUsername(username);
		model.addAttribute("users", users);
		return "topic";
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> getTopicList(Integer page,Integer rows,String queryParams){
		Map<String,Object> results = new HashMap<String, Object>();
		QueryParam[] params = null;
		if(StringUtils.isNotEmpty(queryParams)){
			try {
				params = new ObjectMapper().readValue(queryParams, QueryParam[].class);
			} catch (Throwable e) {
				System.out.println(e.getMessage());
			} 
		}
		IPagination<?> pagination = topicService.getListByPage(page,rows,params==null?null:Arrays.asList(params));
		
		results.put("total", pagination.getTotalCount());
		results.put("rows", pagination.getResults());
		return results;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public Map<String,Object> saveTopic(Topic topic){
		Map<String,Object> results = new HashMap<String, Object>();
		if(topic != null){
			if(topic.getTopicId() == null){
				topic.setCreateTime(new Timestamp(new Date().getTime()));
				topic.setCreateUsername(LoginUserManger.getLoginUsername());
			}else{
				topic.setLastUpdateTime(new Timestamp(new Date().getTime()));
				topic.setLastUpdateUsername(LoginUserManger.getLoginUsername());
			}
		}
		
		try{
			topicService.save(topic);
			results.put("success", true);
			results.put("msg","保存课题成功");
		}catch(ServiceException e){
			results.put("msg","保存课题失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	public Object getTopic(Integer topicId){
		return topicService.getById(topicId);
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String,Object> deleteTopicById(Integer topicId){
		
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			topicService.deleteById(topicId);
			results.put("success", true);
			results.put("msg","删除课题成功");
		}catch(ServiceException e){
			results.put("msg","删除课题失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/export/excel")
	public void exportTopicList(HttpServletRequest request,HttpServletResponse response,String queryParams) throws IOException{
		exportExcel(request, response, queryParams, new BuildExcelCallback() {
			
			@Override
			public BaseService getService() {
				return topicService;
			}
			
			@Override
			public String getFileName() {
				return "在研课题";
			}
			
			@Override
			public String[] getColumnHeads() {
				return new String[]{"姓名","开始时间","结束时间","课题名称","经费","项目来源","主持人","合作人","年度"};
			}

			@Override
			public void fillRowData(HSSFRow row, Object d,CellStyle baseStyle) {
				Topic topic = (Topic)d;
				setCellValue(row,0,topic.getUser().getFullName(),baseStyle);
				setCellValue(row,1,topic.getBeginTime());
				setCellValue(row,2,topic.getEndTime());
				setCellValue(row,3,topic.getTopicName(),baseStyle);
				setCellValue(row,4,topic.getFunds(),baseStyle);
				setCellValue(row,5,topic.getProjectSource(),baseStyle);
				setCellValue(row,6,topic.getPresenter(),baseStyle);
				setCellValue(row,7,topic.getPartner(),baseStyle);
				setCellValue(row,8,topic.getYear(),baseStyle);
			}
		});
	}
}
