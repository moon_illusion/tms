<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north'"
		style="height: 70px; vertical-align: middle; overflow: hidden;">
		<dl id="book_query_form" class="query-form">
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<dt>姓名</dt> 
			<dd>
				<select name="user_id" style="width: 110px;" _param="type:'this_'">
					<option value=""></option>
					<c:forEach items="${users}" var="user">
						<option value="${user.userId}">${user.fullName}</option>
					</c:forEach>
				</select>
			</dd>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user_id" _param="type:'this_'" value="${users[0].userId}" />
			</security:authorize>
			<dt>论著名称</dt>
			<dd>
				<input type="text" name="bookName" id="bookName" _param="relation:'like'" />
			</dd>
			<dt <security:authorize ifAllGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >出版社</dt> 
			<dd><input type="text" name="pubHouse" _param="relation:'like'" /></dd>
			<dt <security:authorize ifNotGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >年度</dt> 
			<dd>
				<select name="year" style="width: 110px;">
					<option value=""></option>
					<c:forEach begin="2010" end="2020" var="item">
						<option value="${item}">${item}</option>
					</c:forEach>
				</select>
			</dd>
			<dd>
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="queryBook()">查询</a>
			<dd>
		</dl>
	</div>
	<div data-options="region:'center'">
		<table id="book_grid" class="easyui-datagrid"
			data-options="
				method:'post',
				url:'${path}/book/list',
				fit:true,
				border:false,
				singleSelect:true,
				idField:'bookId',
				pagination:true,
				toolbar:'#book_grid_toolbar',
				<security:authorize ifNotGranted="ROLE_ADMIN">
				queryParams:{
					queryParams:$.toJSON([{
						name:'user_id',
						type:'this_',
						relation:'=',
						value:'${users[0].userId}'
					}])
				},
				</security:authorize>
				onDblClickRow:function(rowIndex, rowData){
					showModifyBookDialog(rowData);
				}
			">
			<thead>
				<tr>
					<th data-options="field:'user',width:100,align:'center',
						formatter:function(value,row,index){
							if(value){
								return value.fullName;
							}
							return '';
						}">姓名</th>
					<th data-options="field:'bookName',width:150,align:'center'">论著名称</th>
					<th data-options="field:'pubHouse',width:150,align:'center'">出版社</th>
					<th data-options="field:'chiefEditor',width:130,align:'center'">主编</th>
					<th data-options="field:'cooperator',width:130,align:'center'">合作者</th>
					<th data-options="field:'pubTime',width:130,align:'center'">发表时间</th>
					<th data-options="field:'year',width:100,align:'center'">年度</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div id="book_grid_toolbar">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="showAddBookDialog()">新增</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="showModifyBookDialog()">修改</a>
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteBook()">删除</a>
	<a href="#" class="easyui-menubutton" data-options="menu:'#mmbook'">导出</a>
</div>

<div id="book_detail" class="easyui-dialog" style="width:420px;height:380px;padding:10px;"
	data-options="closed:true,cache:false,modal:true,buttons:'#book_detail_buttons'">
	<form id="book_detail_form" method="post">
	<input type="hidden" name="bookId" />
	<dl class="detail-form-list">
		<dt>姓名：</dt> 
		<dd>
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<select name="user" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach items="${users}" var="user">
					<option value="${user.userId}">${user.fullName}</option>
				</c:forEach>
			</select>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user" value="${users[0].userId}"/>
				<input type="text" name="fullName" readonly="readonly" value="${users[0].fullName}"/>
			</security:authorize>
		</dd>
		<dt>论著名称：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="bookName" data-options="required:true"/></dd>
		<dt>出版社：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="pubHouse" data-options="required:true"/></dd>
		<dt>主编：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="chiefEditor" data-options="required:true"/></dd>
		<dt>合作者：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="cooperator" data-options="required:true"/></dd>
		<dt>发表时间：</dt> 
		<dd><input type="text" class="easyui-datebox" name="pubTime" data-options="required:true"/></dd>
		<dt>年度：</dt> 
		<dd>
			<select name="year" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach begin="2010" end="2020" var="item">
					<option value="${item}">${item}</option>
				</c:forEach>
			</select>
		</dd>
		<dt>备注：</dt> 
		<dd><textarea name="memo" rows="3" cols="25"></textarea></dd>
	</dl>
	<input type="hidden" name="createTime" />
	<input type="hidden" name="createUsername" />
</form>
	
</div>

<div id="book_detail_buttons">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveBook()">保存</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#book_detail').dialog('close');">取消</a>
</div>

<div id="mmbook" data-options="onClick:menuBookHandler" style="width:150px;">
	<div data-options="name:'excel'">Excel</div>
</div>

<form id="book_export_form" method="post" action="${path}/book/export/excel">
	<security:authorize ifAllGranted="ROLE_ADMIN">
		<input type="hidden" id="book_query_params" name="queryParams">
	</security:authorize>
	<security:authorize ifNotGranted="ROLE_ADMIN">
		<input type="hidden" id="book_query_params" name="queryParams" value='[{"name":"user_id","type":"this_","relation":"=","value":"${users[0].userId}"}]'>
	</security:authorize>
</form>

<script type="text/javascript">

function menuBookHandler(item){
	if(item.name=='excel'){
		$('#book_export_form').submit();
	}
}
	
function queryBook() {
	var params = [];
	
	$(':input','#book_query_form').each(function(i,v){
		var self = $(this);
		var val = self.val();
		if(val && val!='' ){
			var param = eval('({' + (self.attr('_param') || '') + '})') || {};
			if(!param.name || param.name==''){
				param.name = self.attr('name');
			}
			if(!param.relation || param.relation == ''){
				param.relation = '=';
			}
			param.value = val;
			params.push(param);
		}
	});
	
	
	var _query_params = $.toJSON(params);;
	
	$('#book_query_params').val(_query_params);
	$('#book_grid').datagrid('options').queryParams.queryParams =_query_params; 
	$('#book_grid').datagrid('reload');
}

function showAddBookDialog() {
	$("#book_detail")
		.dialog('setTitle', '新增论著')
		.dialog('open');
	$('#book_detail_form').form('clear');
	$('select','#book_detail_form').each(function(i,v){
		$(this).val($('option',this)[0].value);
	});
	$('input[name="user"]','#book_detail_form').val('${users[0].userId}');
	$('input[name="fullName"]','#book_detail_form').val('${users[0].fullName}');
}

function showModifyBookDialog(rowData) {
	if (!rowData) {
		var row = $('#book_grid').datagrid('getSelected');
		if (row) {
			rowData = row;
		} else {
			$.alert('请选择要修改的记录！');
		}
	}

	if(rowData){
		$("#book_detail")
			.dialog('setTitle', '修改论著')
			.dialog('open');
		var url = '${path}/book/detail?bookId=' + rowData.bookId;
		$.get(url,function(rs){
			if(rs){
				var book = rs;
				var user = rs.user;
				if(user){
					book.user = user.userId;
					book.fullName = user.fullName;
					$('#book_detail_form').form('load',book);
				}
			}
		},'json');
	}
}

/**
 * 
 */
function saveBook() {
	var url = '${path}/book/save'
	
	$('#book_detail_form').form('submit', {
		url : url,
		onSubmit : function() {
			if($('input[name="user"]','#book_detail_form').val() == -1){
				return false;
			}
			if($('input[name="year"]','#book_detail_form').val() == -1){
				return false;
			}
			return $('#book_detail_form').form('validate');
		},
		success : function(data) {
			if(data){
				$('#book_detail').dialog('close');
				$('#book_grid').datagrid('reload');
				var rs = $.evalJSON(data);
				$.message(rs.msg);
			}
		}
	});
}

/**
 * 
 */
function deleteBook() {
	var row = $('#book_grid').datagrid('getSelected');
	var index = $('#book_grid').datagrid('getRowIndex', row);
	if (row) {
		$.confirm('请确认是否删除选择的记录?', function() {
			var url = '${path}/book/delete';
			$.post(url, {
				bookId : row.bookId
			},
			function(rs){
				if(rs.success){
					$.message(rs.msg || '删除论著成功！');
					$('#book_grid').datagrid('reload');
				}else{
					$.message(rs.msg || '删除论著失败！');
				}
			},'json');
		});
	} else {
		$.alert('请选择要删除的记录！');
	}
}
</script>