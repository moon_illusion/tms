package org.aid.tms.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aid.commons.IPagination;
import org.aid.commons.LoginUserManger;
import org.aid.tms.model.TEducation;
import org.aid.tms.model.TResume;
import org.aid.tms.model.TUser;
import org.aid.tms.service.BaseService;
import org.aid.tms.service.ResumeService;
import org.aid.tms.service.ServiceException;
import org.aid.tms.service.UserService;
import org.aid.tms.vo.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/resume")
public class ResumeController extends BaseController{
	@Autowired
	private ResumeService resumeService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/view")
	public String goResumePage(SecurityContextHolderAwareRequestWrapper request,Model model){
		String username = null;
		if(!request.isUserInRole("ROLE_ADMIN")){
			username = LoginUserManger.getLoginUsername();
		}
		List<TUser> users = userService.getUsersByUsername(username);
		model.addAttribute("users", users);
		return "resume";
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> getResumeList(Integer page,Integer rows,String queryParams){
		Map<String,Object> results = new HashMap<String, Object>();
		QueryParam[] params = null;
		if(StringUtils.isNotEmpty(queryParams)){
			try {
				params = new ObjectMapper().readValue(queryParams, QueryParam[].class);
			} catch (Throwable e) {
				System.out.println(e.getMessage());
			} 
		}
		IPagination<?> pagination = resumeService.getListByPage(page,rows,params==null?null:Arrays.asList(params));
		
		results.put("total", pagination.getTotalCount());
		results.put("rows", pagination.getResults());
		return results;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public Map<String,Object> saveResume(TResume resume){
		Map<String,Object> results = new HashMap<String, Object>();
		if(resume != null){
			if(resume.getResumeId() == null){
				resume.setCreateTime(new Timestamp(new Date().getTime()));
				resume.setCreateUsername(LoginUserManger.getLoginUsername());
			}else{
				resume.setLastUpdateTime(new Timestamp(new Date().getTime()));
				resume.setLastUpdateUsername(LoginUserManger.getLoginUsername());
			}
		}
		
		try{
			resumeService.save(resume);
			results.put("success", true);
			results.put("msg","保存简历成功");
		}catch(ServiceException e){
			results.put("msg","保存简历失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	public Object getResume(Integer resumeId){
		return resumeService.getById(resumeId);
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String,Object> deleteResumeById(Integer resumeId){
		
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			resumeService.deleteById(resumeId);
			results.put("success", true);
			results.put("msg","删除简历成功");
		}catch(ServiceException e){
			results.put("msg","删除简历失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/export/excel")
	public void exportResumeList(HttpServletRequest request,HttpServletResponse response,String queryParams) throws IOException{
		exportExcel(request, response, queryParams, new BuildExcelCallback() {
			
			@Override
			public BaseService getService() {
				return resumeService;
			}
			
			@Override
			public String getFileName() {
				return "工作简历";
			}
			
			@Override
			public String[] getColumnHeads() {
				return new String[]{"姓名","开始年月","结束年月","工作单位","职务","职称","年度"};
			}

			@Override
			public void fillRowData(HSSFRow row, Object d,CellStyle baseStyle) {
				TResume r = (TResume)d;
				setCellValue(row,0,r.getUser().getFullName(),baseStyle);
				setCellValue(row,1,r.getBeginTime());
				setCellValue(row,2,r.getEndTime());
				setCellValue(row,3,r.getDanwei(),baseStyle);
				setCellValue(row,4,r.getPost(),baseStyle);
				setCellValue(row,5,r.getTitle(),baseStyle);
				setCellValue(row,6,r.getYear(),baseStyle);
			}
		});
	}
}
