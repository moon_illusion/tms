package org.aid.tms.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aid.tms.vo.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.codehaus.jackson.map.ObjectMapper;

public class BaseController {
	
	protected void exportExcel(HttpServletRequest request,HttpServletResponse response,String queryParams,BuildExcelCallback callback) throws IOException{
		
		if(callback == null){
			return;
		}
		
		QueryParam[] params = null;
		if(StringUtils.isNotEmpty(queryParams)){
			try {
				params = new ObjectMapper().readValue(queryParams, QueryParam[].class);
			} catch (Throwable e) {
				System.out.println(e.getMessage());
			} 
		}
		
		List<QueryParam> pl = (params==null?null:Arrays.asList(params));
		
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet(callback.getFileName());
		HSSFRow row = sheet.createRow(0);
		int j=0;
		
		CellStyle baseStyle = createBorderedStyle(wb);
		
		Font headerFont = wb.createFont();  
        headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		CellStyle headStyle = createBorderedStyle(wb);
		headStyle.setAlignment(CellStyle.ALIGN_CENTER);  
		headStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());  
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);  
		headStyle.setFont(headerFont);
		
		for(String hc : callback.getColumnHeads()){
			HSSFCell cell = row.createCell(j++, Cell.CELL_TYPE_STRING);
			cell.setCellValue(hc);
	        cell.setCellStyle(headStyle);
		}
		
		List datas = callback.getService().getList(pl);
		
		if(datas != null){
			int i = 1;
			for(Object d : datas){
				HSSFRow r = sheet.createRow(i++);
				callback.fillRowData(r,d,baseStyle);
			}
		}
		
		printSetup(sheet);
		String fileName = encodeFilename(callback.getFileName(), request);
		response.reset();
		response.setContentType("application/vnd.ms-excel;charset=UTF-8");
		response.setHeader("Content-disposition","attachment;filename="+fileName);
		
		OutputStream out = response.getOutputStream();
		wb.write(out);
		out.flush();
		out.close();
		out = null;
	}

	private String encodeFilename(String fileName, HttpServletRequest request) {
		try {
			String ua = request.getHeader("User-Agent").toLowerCase();
			if (ua.indexOf("firefox") > 0) {
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");// firefox浏览器
			} else if (ua.indexOf("MSIE") > 0) {
				fileName = URLEncoder.encode(fileName, "UTF-8");
			}
		} catch (Exception e) {
		}

		return fileName;
	}
	
	private void printSetup(HSSFSheet sheet){
		HSSFPrintSetup hps = sheet.getPrintSetup(); 
        hps.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE); // 设置A4纸
        hps.setLandscape(false); // 将页面设置为横向打印模式
		sheet.setHorizontallyCenter(true); // 设置打印页面为水平居中
		//sheet.setVerticallyCenter(true); // 设置打印页面为垂直居中
        //冻结第一行和第二行
        sheet.createFreezePane( 0, 1, 0, 1 );
	}
	
	protected CellStyle createBorderedStyle(Workbook wb){  
        CellStyle style = wb.createCellStyle();  
        style.setBorderRight(CellStyle.BORDER_THIN);  
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());  
        style.setBorderBottom(CellStyle.BORDER_THIN);  
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());  
        style.setBorderLeft(CellStyle.BORDER_THIN);  
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());  
        style.setBorderTop(CellStyle.BORDER_THIN);  
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setWrapText(true);
        return style;  
    }
	
	protected void setCellValue(HSSFRow row,int cellIndex,String value,CellStyle style) {
		HSSFCell cell = row.createCell(cellIndex, Cell.CELL_TYPE_STRING);
		cell.setCellStyle(style);
		if(value != null){
			cell.setCellValue(value);
		}
	}
	
	
	protected void setCellValue(HSSFRow row,int cellIndex,Date value) {
		HSSFCell cell = row.createCell(cellIndex);
		CellStyle style = createBorderedStyle(row.getSheet().getWorkbook());
		HSSFDataFormat format= row.getSheet().getWorkbook().createDataFormat();
		style.setDataFormat(format.getFormat("yyyy-mm-dd"));
		cell.setCellStyle(style);
		
		if(value != null){
			cell.setCellValue(value);
		}
	}
	
	protected void setCellValue(HSSFRow row,int cellIndex,Number value,CellStyle style) {
		HSSFCell cell = row.createCell(cellIndex, Cell.CELL_TYPE_NUMERIC);
		cell.setCellStyle(style);
		if(value != null){
			cell.setCellValue(value.doubleValue());
		}
	}
	
	protected void setCellValue(HSSFRow row,int cellIndex,boolean value,CellStyle style) {
		HSSFCell cell = row.createCell(cellIndex, Cell.CELL_TYPE_BOOLEAN);
		cell.setCellValue(value);
		cell.setCellStyle(style);
	}
}
