package org.aid.commons;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.aid.tms.model.TUser;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.WebRequest;

public class MyBindingInitializer implements WebBindingInitializer {
	private static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
	private static final String DEFAULT_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
	

	@Override
	public void initBinder(WebDataBinder binder, WebRequest request) {
		binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(DEFAULT_DATE_PATTERN,DEFAULT_DATETIME_PATTERN,true));
		binder.registerCustomEditor(TUser.class,new TUserPropEditor(true));
	}
}

class CustomDateEditor extends PropertyEditorSupport {

	private String datePattern;
	private String dateTimePattern;
	private final boolean allowEmpty;

	public CustomDateEditor(String datePattern,String dateTimePattern, boolean allowEmpty) {
		this.datePattern = datePattern;
		this.dateTimePattern = dateTimePattern;
		this.allowEmpty = allowEmpty;
	}

	public void setAsText(String text) throws IllegalArgumentException {
		if ((this.allowEmpty) && (!(StringUtils.hasText(text)))) {
			setValue(null);
		} else {
			SimpleDateFormat formater = null;
			if (text != null){
				if(text.length() > 10){
					formater = new SimpleDateFormat(dateTimePattern);
				}else{
					formater = new SimpleDateFormat(datePattern);
				}
			}
			try {
				setValue(formater.parse(text));
			} catch (ParseException ex) {
				throw new IllegalArgumentException("Could not parse date: "
						+ ex.getMessage(), ex);
			}
		}
	}

	public String getAsText() {
		Date value = (Date) getValue();
		return ((value != null) ? new SimpleDateFormat(datePattern).format(value) : "");
	}
}

class TUserPropEditor extends PropertyEditorSupport{
	
	private final boolean allowEmpty;
	
	public TUserPropEditor(boolean allowEmpty){
		this.allowEmpty = allowEmpty;
	}
	
	public void setAsText(String text) throws IllegalArgumentException {
		if ((this.allowEmpty) && (!(StringUtils.hasText(text)))) {
			setValue(null);
		} else {
			try {
				TUser u = new TUser();
				u.setUserId(Integer.valueOf(text));
				setValue(u);
			} catch (NumberFormatException ex) {
				setValue(null);
			}
		}
	}

	public String getAsText() {
		TUser value = (TUser) getValue();
		return ((value != null) ? value.getUserId() + "" : "");
	}
}
