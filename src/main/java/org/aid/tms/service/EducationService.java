package org.aid.tms.service;

import org.aid.tms.dao.hibernate.BaseDao;
import org.aid.tms.dao.hibernate.EducationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EducationService extends BaseService {
	
	@Autowired
	private EducationDao educationDao;
	
	@Override
	public BaseDao getBaseDao() {
		return educationDao;
	}

	@Override
	public String getObjName() {
		return "教育背景";
	}

}
