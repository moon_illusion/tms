<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north'"
		style="height: 70px; vertical-align: middle; overflow: hidden;">
		<dl id="education_query_form" class="query-form">
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<dt>姓名</dt> 
			<dd>
				<select name="user_id" style="width: 110px;" _param="type:'this_'">
					<option value=""></option>
					<c:forEach items="${users}" var="user">
						<option value="${user.userId}">${user.fullName}</option>
					</c:forEach>
				</select>
			</dd>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user_id" _param="type:'this_'" value="${users[0].userId}" />
			</security:authorize>
			<dt>毕业院校</dt>
			<dd>
				<input type="text" name="college" id="major" _param="relation:'like'" />
			</dd>
			<dt <security:authorize ifAllGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >专业</dt> 
			<dd><input type="text" name="major" _param="relation:'like'" /></dd>
			<dt <security:authorize ifNotGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >年度</dt> 
			<dd>
				<select name="year" style="width: 110px;">
					<option value=""></option>
					<c:forEach begin="2010" end="2020" var="item">
						<option value="${item}">${item}</option>
					</c:forEach>
				</select>
			</dd>
			<dd>
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="queryEducation()">查询</a>
			<dd>
		</dl>
	</div>
	<div data-options="region:'center'">
		<table id="education_grid" class="easyui-datagrid"
			data-options="
				method:'post',
				url:'${path}/education/list',
				fit:true,
				border:false,
				singleSelect:true,
				idField:'educationId',
				pagination:true,
				toolbar:'#education_grid_toolbar',
				<security:authorize ifNotGranted="ROLE_ADMIN">
				queryParams:{
					queryParams:$.toJSON([{
						name:'user_id',
						type:'this_',
						relation:'=',
						value:'${users[0].userId}'
					}])
				},
				</security:authorize>
				onDblClickRow:function(rowIndex, rowData){
					showModifyEducationDialog(rowData);
				}
			">
			<thead>
				<tr>
					<th data-options="field:'user',width:100,align:'center',
						formatter:function(value,row,index){
							if(value){
								return value.fullName;
							}
							return '';
						}">姓名</th>
					<th data-options="field:'beginTime',width:150,align:'center',
						formatter:function(value,row,index){
							if(row){
								return value + '—' + row.endTime;
							}
							return value + '至今';
						}
					">起止时间</th>
					<th data-options="field:'college',width:150,align:'center'">毕业院校</th>
					<th data-options="field:'major',width:150,align:'center'">专业</th>
					<th data-options="field:'degree',width:150,align:'center'">获得学位</th>
					<th data-options="field:'year',width:100,align:'center'">年度</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div id="education_grid_toolbar">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="showAddEducationDialog()">新增</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="showModifyEducationDialog()">修改</a>
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteEducation()">删除</a>
	<a href="#" class="easyui-menubutton" data-options="menu:'#mmeducation'">导出</a>
</div>

<div id="education_detail" class="easyui-dialog" style="width:400px;height:320px;padding:10px;"
	data-options="closed:true,cache:false,modal:true,buttons:'#education_detail_buttons'">
	<form id="education_detail_form" method="post">
	<input type="hidden" name="educationId" />
	<dl class="detail-form-list">
		<dt>姓名：</dt> 
		<dd>
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<select name="user" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach items="${users}" var="user">
					<option value="${user.userId}">${user.fullName}</option>
				</c:forEach>
			</select>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user" value="${users[0].userId}"/>
				<input type="text" name="fullName" readonly="readonly" value="${users[0].fullName}"/>
			</security:authorize>
		</dd>
		<dt>开始时间：</dt> 
		<dd><input type="text" class="easyui-datebox" name="beginTime" data-options="required:true" />
		<dt>结束时间：</dt> 
		<dd><input type="text" class="easyui-datebox" name="endTime" data-options="required:true" /></dd>
		<dt>毕业院校：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="college" data-options="required:true"/></dd>
		<dt>专业：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="major" data-options="required:true"/></dd>
		<dt>学位：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="degree" data-options="required:true"/></dd>
		<dt>年度：</dt> 
		<dd>
			<select name="year" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach begin="2010" end="2020" var="item">
					<option value="${item}">${item}</option>
				</c:forEach>
			</select>
		</dd>
	</dl>
	<input type="hidden" name="createTime" />
	<input type="hidden" name="createUsername" />
</form>
	
</div>

<div id="education_detail_buttons">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveEducation()">保存</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#education_detail').dialog('close');">取消</a>
</div>

<div id="mmeducation" data-options="onClick:menuEducationHandler" style="width:150px;">
	<div data-options="name:'excel'">Excel</div>
</div>

<form id="education_export_form" method="post" action="${path}/education/export/excel">
	<security:authorize ifAllGranted="ROLE_ADMIN">
		<input type="hidden" id="education_query_params" name="queryParams">
	</security:authorize>
	<security:authorize ifNotGranted="ROLE_ADMIN">
		<input type="hidden" id="education_query_params" name="queryParams" value='[{"name":"user_id","type":"this_","relation":"=","value":"${users[0].userId}"}]'>
	</security:authorize>
</form>

<script type="text/javascript">

function menuEducationHandler(item){
	if(item.name=='excel'){
		$('#education_export_form').submit();
	}
}
function queryEducation() {
	var params = [];
	
	$(':input','#education_query_form').each(function(i,v){
		var self = $(this);
		var val = self.val();
		if(val && val!='' ){
			var param = eval('({' + (self.attr('_param') || '') + '})') || {};
			if(!param.name || param.name==''){
				param.name = self.attr('name');
			}
			if(!param.relation || param.relation == ''){
				param.relation = '=';
			}
			param.value = val;
			params.push(param);
		}
	});
	
	var _query_params = $.toJSON(params);;
	
	$('#education_query_params').val(_query_params);
	$('#education_grid').datagrid('options').queryParams.queryParams =_query_params;
	$('#education_grid').datagrid('reload');
}

function showAddEducationDialog() {
	$("#education_detail")
		.dialog('setTitle', '新增教育背景')
		.dialog('open');
	$('#education_detail_form').form('clear');
	$('select','#education_detail_form').each(function(i,v){
		$(this).val($('option',this)[0].value);
	});
	$('input[name="user"]','#education_detail_form').val('${users[0].userId}');
	$('input[name="fullName"]','#education_detail_form').val('${users[0].fullName}');
}

function showModifyEducationDialog(rowData) {
	if (!rowData) {
		var row = $('#education_grid').datagrid('getSelected');
		if (row) {
			rowData = row;
		} else {
			$.alert('请选择要修改的记录！');
		}
	}

	if(rowData){
		$("#education_detail")
			.dialog('setTitle', '修改教育背景')
			.dialog('open');
		var url = '${path}/education/detail?educationId=' + rowData.educationId;
		$.get(url,function(rs){
			if(rs){
				var education = rs;
				var user = rs.user;
				if(user){
					education.user = user.userId;
					education.fullName = user.fullName;
					$('#education_detail_form').form('load',education);
				}
			}
		},'json');
	}
}

/**
 * 
 */
function saveEducation() {
	var url = '${path}/education/save'
	
	$('#education_detail_form').form('submit', {
		url : url,
		onSubmit : function() {
			if($('input[name="user"]','#education_detail_form').val() == -1){
				return false;
			}
			if($('input[name="year"]','#education_detail_form').val() == -1){
				return false;
			}
			return $('#education_detail_form').form('validate');
		},
		success : function(data) {
			if(data){
				$('#education_detail').dialog('close');
				$('#education_grid').datagrid('reload');
				var rs = $.evalJSON(data);
				$.message(rs.msg);
			}
		}
	});
}

/**
 * 
 */
function deleteEducation() {
	var row = $('#education_grid').datagrid('getSelected');
	var index = $('#education_grid').datagrid('getRowIndex', row);
	if (row) {
		$.confirm('请确认是否删除选择的记录?', function() {
			var url = '${path}/education/delete';
			$.post(url, {
				educationId : row.educationId
			},
			function(rs){
				if(rs.success){
					$.message(rs.msg || '删除教育背景成功！');
					$('#education_grid').datagrid('reload');
				}else{
					$.message(rs.msg || '删除教育背景失败！');
				}
			},'json');
		});
	} else {
		$.alert('请选择要删除的记录！');
	}
}
</script>