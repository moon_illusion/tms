$.extend({
	// 项目的basePath
	bp : function() {
		var curWwwPath = window.document.location.href;
		var pathName = window.document.location.pathname;
		var pos = curWwwPath.indexOf(pathName);
		var localhostPaht = curWwwPath.substring(0, pos);
		var projectName = pathName.substring(0,pathName.substr(1).indexOf('/') + 1);
		return (localhostPaht + projectName);
	},

	ns : function() {
		var o = {}, d;
		for ( var i = 0; i < arguments.length; i++) {
			d = arguments[i].split(".");
			o = window[d[0]] = window[d[0]] || {};
			for ( var k = 0; k < d.slice(1).length; k++) {
				o = o[d[k + 1]] = o[d[k + 1]] || {};
			}
		}
		return o;
	},
	
	urlparam : function(name){
		var url = decodeURI(location.href);
		var search = decodeURI(location.search);
		
		var params = {};
		if (search.indexOf("?") != -1) {
			var str = search.substr(1);
			strs = str.split("&");
			for(var i = 0; i < strs.length; i ++) {
				params[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
			}
		}
		
		return params[name];
	},
	
	alert : function(msg){
		$.messager.alert('提示', msg, 'info');
	},

	message : function(msg) {
		if (!msg)
			return;

		$.messager.show({
			title : '信息',
			msg : msg,
			showType : 'slide'
		});
	},

	confirm : function(msg, okHandler) {
		$.messager.confirm('请确认', msg, function(r) {
			if(r && okHandler){
				okHandler();
			}
		});
	}
});


$.ajaxSetup({
	error: function (XMLHttpRequest, textStatus, errorThrown){
		if(XMLHttpRequest.status==403){
			alert('您没有权限访问此资源或进行此操作');
			return false;
		}
	},
	contentType:"application/x-www-form-urlencoded;charset=utf-8",
	complete : function(XMLHttpRequest, textStatus) {
		var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus"); // 通过XMLHttpRequest取得响应头，sessionstatus，
		if (sessionstatus == "timeout") {
			// 如果超时就处理 ，指定要跳转的页面
			top.location.href = $.bp() + "/login.html";
		}
	}   
});

$.extend($.fn.tabs.methods, {   
    /**
     * tabs组件每个tab panel对应的小工具条绑定的事件没有传递事件参数  
     * 本函数修正这个问题  
     * @param {[type]} jq [description]  
     */  
    addEventParam: function(jq) {   
        return jq.each(function() {   
            var that = this;   
            var headers = $(this).find('>div.tabs-header>div.tabs-wrap>ul.tabs>li');   
            headers.each(function(i) {   
                var tools = $(that).tabs('getTab', i).panel('options').tools;   
                if (typeof tools != "string") {   
                    $(this).find('>span.tabs-p-tool a').each(function(j) {   
                        $(this).unbind('click').bind("click", {   
                            handler: tools[j].handler   
                        }, function(e) {   
                            if ($(this).parents("li").hasClass("tabs-disabled")) {   
                                return;   
                            }   
                            e.data.handler.call(this, e);   
                        });   
                    });   
                }   
            })   
        });   
    }   
});