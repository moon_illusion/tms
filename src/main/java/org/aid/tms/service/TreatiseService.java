package org.aid.tms.service;

import org.aid.tms.dao.hibernate.BaseDao;
import org.aid.tms.dao.hibernate.TreatiseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TreatiseService extends BaseService {

	@Autowired
	private TreatiseDao treatiseDao;
	
	@Override
	public BaseDao getBaseDao() {
		return treatiseDao;
	}

	@Override
	public String getObjName() {
		return "论文";
	}
}
