package org.aid.tms.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.aid.commons.DateTimeSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * @author lee
 * 学科建设
 */
@Entity
@Table(name="t_subject")
public class TSubject {
	private Integer subjectId;
	
	private String subjectName;
	
	private Date beginTime;
	private Date endTime;
	
	private String jobContent;
	private Integer bilingual;
	private String major;
	private Integer classHour;
	private Integer classUnit;
	
	private Integer personNum;
	
	private String year;
	
	private Date createTime;
	private String createUsername;
	
	private Date lastUpdateTime;
	private String lastUpdateUsername;
	
	private TUser user;
	
	@Id
	@Column(name = "subject_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

	@Column(name="subject_name")
	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	@Column(name="begin_time")
	@Temporal(TemporalType.DATE)
	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	@Column(name="end_time")
	@Temporal(TemporalType.DATE)
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	@Column(name="job_content")
	public String getJobContent() {
		return jobContent;
	}

	public void setJobContent(String jobContent) {
		this.jobContent = jobContent;
	}

	@Column(name="bilingual")
	public Integer getBilingual() {
		return bilingual;
	}

	public void setBilingual(Integer bilingual) {
		this.bilingual = bilingual;
	}

	@Column
	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	@Column(name="class_hour")
	public Integer getClassHour() {
		return classHour;
	}

	public void setClassHour(Integer classHour) {
		this.classHour = classHour;
	}

	@Column(name="class_unit")
	public Integer getClassUnit() {
		return classUnit;
	}

	public void setClassUnit(Integer classUnit) {
		this.classUnit = classUnit;
	}

	@Column(name="person_num")
	public Integer getPersonNum() {
		return personNum;
	}

	public void setPersonNum(Integer personNum) {
		this.personNum = personNum;
	}

	@Column
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@JsonSerialize(using=DateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name="create_username")
	public String getCreateUsername() {
		return createUsername;
	}

	public void setCreateUsername(String createUsername) {
		this.createUsername = createUsername;
	}

	@JsonSerialize(using=DateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_update_time")
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	@Column(name="last_update_username")
	public String getLastUpdateUsername() {
		return lastUpdateUsername;
	}

	public void setLastUpdateUsername(String lastUpdateUsername) {
		this.lastUpdateUsername = lastUpdateUsername;
	}

	@ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH }, optional = true)  
    @JoinColumn(name="user_id")
	public TUser getUser() {
		return user;
	}

	public void setUser(TUser user) {
		this.user = user;
	}
}
