package org.aid.tms.dao.hibernate;

import org.aid.tms.model.TResume;
import org.springframework.stereotype.Repository;

@Repository
public class ResumeDao extends BaseDao<TResume, Integer> {

	@Override
	protected Class<TResume> getEntityClass() {
		return TResume.class;
	}
}
