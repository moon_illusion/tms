package org.aid.tms.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aid.commons.IPagination;
import org.aid.commons.LoginUserManger;
import org.aid.tms.model.TBook;
import org.aid.tms.model.TUser;
import org.aid.tms.service.BaseService;
import org.aid.tms.service.BookService;
import org.aid.tms.service.ServiceException;
import org.aid.tms.service.UserService;
import org.aid.tms.vo.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/book")
public class BookController extends BaseController{
	public static String[] TABLE_HEAD = new String[]{"姓名","论著名称"};
	
	@Autowired
	private BookService bookService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/view")
	public String goBookPage(SecurityContextHolderAwareRequestWrapper request,Model model){
		String username = null;
		if(!request.isUserInRole("ROLE_ADMIN")){
			username = LoginUserManger.getLoginUsername();
		}
		List<TUser> users = userService.getUsersByUsername(username);
		model.addAttribute("users", users);
		return "book";
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> getBookList(Integer page,Integer rows,String queryParams){
		Map<String,Object> results = new HashMap<String, Object>();
		QueryParam[] params = null;
		if(StringUtils.isNotEmpty(queryParams)){
			try {
				params = new ObjectMapper().readValue(queryParams, QueryParam[].class);
			} catch (Throwable e) {
				System.out.println(e.getMessage());
			} 
		}
		IPagination<?> pagination = bookService.getListByPage(page,rows,params==null?null:Arrays.asList(params));
		
		results.put("total", pagination.getTotalCount());
		results.put("rows", pagination.getResults());
		return results;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public Map<String,Object> saveBook(TBook book){
		Map<String,Object> results = new HashMap<String, Object>();
		if(book != null){
			if(book.getBookId() == null){
				book.setCreateTime(new Timestamp(new Date().getTime()));
				book.setCreateUsername(LoginUserManger.getLoginUsername());
			}else{
				book.setLastUpdateTime(new Timestamp(new Date().getTime()));
				book.setLastUpdateUsername(LoginUserManger.getLoginUsername());
			}
		}
		
		try{
			bookService.save(book);
			results.put("success", true);
			results.put("msg","保存论著成功");
		}catch(ServiceException e){
			results.put("msg","保存论著失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	public Object getBook(Integer bookId){
		return bookService.getById(bookId);
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String,Object> deleteBookById(Integer bookId){
		
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			bookService.deleteById(bookId);
			results.put("success", true);
			results.put("msg","删除论著成功");
		}catch(ServiceException e){
			results.put("msg","删除论著失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/export/excel")
	public void exportBookList(HttpServletRequest request,HttpServletResponse response,String queryParams) throws IOException{
		exportExcel(request, response, queryParams, new BuildExcelCallback() {
			
			@Override
			public BaseService getService() {
				return bookService;
			}
			
			@Override
			public String getFileName() {
				return "发表论著";
			}
			
			@Override
			public String[] getColumnHeads() {
				return new String[]{"姓名","论著名称","出版社","主编","合作者","发表时间","年度"};
			}

			@Override
			public void fillRowData(HSSFRow row, Object d,CellStyle baseStyle) {
				TBook b = (TBook)d;
				setCellValue(row,0,b.getUser().getFullName(),baseStyle);
				setCellValue(row,1,b.getBookName(),baseStyle);
				setCellValue(row,2,b.getPubHouse(),baseStyle);
				setCellValue(row,3,b.getChiefEditor(),baseStyle);
				setCellValue(row,4,b.getCooperator(),baseStyle);
				setCellValue(row,5,b.getPubTime());
				setCellValue(row,6,b.getYear(),baseStyle);
			}
		});
	}
}
