package org.aid.tms.dao.hibernate;

import org.aid.tms.model.TBook;
import org.springframework.stereotype.Repository;

@Repository
public class BookDao extends BaseDao<TBook, Integer> {

	@Override
	protected Class<TBook> getEntityClass() {
		return TBook.class;
	}
}
