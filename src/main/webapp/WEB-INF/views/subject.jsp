<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north'"
		style="height: 70px; vertical-align: middle; overflow: hidden;">
		<dl id="subject_query_form" class="query-form">
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<dt>姓名</dt> 
			<dd>
				<select name="user_id" style="width: 110px;" _param="type:'this_'">
					<option value=""></option>
					<c:forEach items="${users}" var="user">
						<option value="${user.userId}">${user.fullName}</option>
					</c:forEach>
				</select>
			</dd>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user_id" _param="type:'this_'" value="${users[0].userId}" />
			</security:authorize>
			<dt>课程（实验）名称</dt>
			<dd>
				<input type="text" name="subjectName" id="subjectName" _param="relation:'like'" />
			</dd>
			<dt <security:authorize ifAllGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >授课专业</dt> 
			<dd><input type="text" name="major" _param="relation:'like'" /></dd>
			<dt <security:authorize ifNotGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >年度</dt> 
			<dd>
				<select name="year" style="width: 110px;">
					<option value=""></option>
					<c:forEach begin="2010" end="2020" var="item">
						<option value="${item}">${item}</option>
					</c:forEach>
				</select>
			</dd>
			<dd>
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="querySubject()">查询</a>
			<dd>
		</dl>
	</div>
	<div data-options="region:'center'">
		<table id="subject_grid" class="easyui-datagrid"
			data-options="
				method:'post',
				url:'${path}/subject/list',
				fit:true,
				border:false,
				singleSelect:true,
				idField:'subjectId',
				pagination:true,
				toolbar:'#subject_grid_toolbar',
				<security:authorize ifNotGranted="ROLE_ADMIN">
				queryParams:{
					queryParams:$.toJSON([{
						name:'user_id',
						type:'this_',
						relation:'=',
						value:'${users[0].userId}'
					}])
				},
				</security:authorize>
				onDblClickRow:function(rowIndex, rowData){
					showModifySubjectDialog(rowData);
				}
			">
			<thead>
				<tr>
					<th data-options="field:'user',width:100,align:'center',
						formatter:function(value,row,index){
							if(value){
								return value.fullName;
							}
							return '';
						}">姓名</th>
					<th data-options="field:'beginTime',width:160,align:'center',
						formatter:function(value,row,index){
							if(row){
								return value + '—' + row.endTime;
							}
							return value + '至今';
						}
					">起止时间</th>
					<th data-options="field:'subjectName',width:160,align:'center'">课程（实验）名称</th>
					<th data-options="field:'bilingual',width:100,align:'center',
						formatter:function(value,row,index){
							return value==1?'是':'否';
						}
					">是否双语</th>
					<th data-options="field:'major',width:150,align:'center'">授课专业</th>
					<th data-options="field:'classHour',width:100,align:'center',
						formatter:function(value,row,index){
							if(row){
								return value + (row.classUnit==1?'学时':'周');
							}
							return value + '学时';
						}
					">学时或周数</th>
					<th data-options="field:'personNum',width:100,align:'center'">人数</th>
					<th data-options="field:'year',width:100,align:'center'">年度</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div id="subject_grid_toolbar">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="showAddSubjectDialog()">新增</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="showModifySubjectDialog()">修改</a>
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteSubject()">删除</a>
	<a href="#" class="easyui-menubutton" data-options="menu:'#mmsubject'">导出</a>
</div>

<div id="subject_detail" class="easyui-dialog" style="width:480px;height:460px;padding:10px;"
	data-options="closed:true,cache:false,modal:true,buttons:'#subject_detail_buttons'">
	<form id="subject_detail_form" method="post">
	<input type="hidden" name="subjectId" />
	<dl class="detail-form-list">
		<dt>姓名：</dt> 
		<dd>
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<select name="user" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach items="${users}" var="user">
					<option value="${user.userId}">${user.fullName}</option>
				</c:forEach>
			</select>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user" value="${users[0].userId}"/>
				<input type="text" name="fullName" readonly="readonly" value="${users[0].fullName}"/>
			</security:authorize>
		</dd>
		<dt>开始时间：</dt> 
		<dd><input type="text" class="easyui-datebox" name="beginTime" data-options="required:true" />
		<dt>结束时间：</dt> 
		<dd><input type="text" class="easyui-datebox" name="endTime" data-options="required:true" /></dd>
		<dt>课程（实验）名称：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="subjectName" data-options="required:true"/></dd>
		<dt>是否双语：</dt> 
		<dd><label><input type="radio" name="bilingual" value="1" />是</label><label><input type="radio" name="bilingual" value="0" />否</label></dd>
		<dt>授课专业：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="major" data-options="required:true"/></dd>
		<dt>学时（周数）：</dt> 
		<dd>
			<input type="text" style="width:80px;float: left;" class="easyui-numberbox" name="classHour" data-options="required:true"/>
			<select style="float: left;" name="classUnit">
				<option value="1" selected="selected">学时</option>
				<option value="2">周</option>
			</select>
		</dd>
		<dt>人数：</dt> 
		<dd><input type="text" class="easyui-numberbox" name="personNum" data-options="required:true"/></dd>
		<dt>年度：</dt> 
		<dd>
			<select name="year" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach begin="2010" end="2020" var="item">
					<option value="${item}">${item}</option>
				</c:forEach>
			</select>
		</dd>
		<dt>工作内容（描述）：</dt> 
		<dd><textarea name="jobContent" rows="3" cols="25"></textarea></dd>
	</dl>
	<input type="hidden" name="createTime" />
	<input type="hidden" name="createUsername" />
</form>
	
</div>

<div id="subject_detail_buttons">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveSubject()">保存</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#subject_detail').dialog('close');">取消</a>
</div>

<div id="mmsubject" data-options="onClick:menuSubjectHandler" style="width:150px;">
	<div data-options="name:'excel'">Excel</div>
</div>

<form id="subject_export_form" method="post" action="${path}/subject/export/excel">
	<security:authorize ifAllGranted="ROLE_ADMIN">
		<input type="hidden" id="subject_query_params" name="queryParams">
	</security:authorize>
	<security:authorize ifNotGranted="ROLE_ADMIN">
		<input type="hidden" id="subject_query_params" name="queryParams" value='[{"name":"user_id","type":"this_","relation":"=","value":"${users[0].userId}"}]'>
	</security:authorize>
</form>

<script type="text/javascript">

function menuSubjectHandler(item){
	if(item.name=='excel'){
		$('#subject_export_form').submit();
	}
}
	
function querySubject() {
	var params = [];
	
	$(':input','#subject_query_form').each(function(i,v){
		var self = $(this);
		var val = self.val();
		if(val && val!='' ){
			var param = eval('({' + (self.attr('_param') || '') + '})') || {};
			if(!param.name || param.name==''){
				param.name = self.attr('name');
			}
			if(!param.relation || param.relation == ''){
				param.relation = '=';
			}
			param.value = val;
			params.push(param);
		}
	});
	
	var _query_params = $.toJSON(params);;
	
	$('#subject_query_params').val(_query_params);
	$('#subject_grid').datagrid('options').queryParams.queryParams =_query_params;
	$('#subject_grid').datagrid('reload');
}

function showAddSubjectDialog() {
	$("#subject_detail")
		.dialog('setTitle', '新增科目')
		.dialog('open');
	$('#subject_detail_form').form('clear');
	$('select','#subject_detail_form').each(function(i,v){
		$(this).val($('option',this)[0].value);
	});
	$(':radio[name="bilingual"]','#subject_detail_form')[0].checked = true;
	$('input[name="user"]','#subject_detail_form').val('${users[0].userId}');
	$('input[name="fullName"]','#subject_detail_form').val('${users[0].fullName}');
}

function showModifySubjectDialog(rowData) {
	if (!rowData) {
		var row = $('#subject_grid').datagrid('getSelected');
		if (row) {
			rowData = row;
		} else {
			$.alert('请选择要修改的记录！');
		}
	}

	if(rowData){
		$("#subject_detail")
			.dialog('setTitle', '修改科目')
			.dialog('open');
		var url = '${path}/subject/detail?subjectId=' + rowData.subjectId;
		$.get(url,function(rs){
			if(rs){
				var subject = rs;
				var user = rs.user;
				if(user){
					subject.user = user.userId;
					subject.fullName = user.fullName;
					$('#subject_detail_form').form('load',subject);
				}
			}
		},'json');
	}
}

/**
 * 
 */
function saveSubject() {
	var url = '${path}/subject/save'
	
	$('#subject_detail_form').form('submit', {
		url : url,
		onSubmit : function() {
			if($('input[name="user"]','#subject_detail_form').val() == -1){
				return false;
			}
			if($('input[name="year"]','#subject_detail_form').val() == -1){
				return false;
			}
			return $('#subject_detail_form').form('validate');
		},
		success : function(data) {
			if(data){
				$('#subject_detail').dialog('close');
				$('#subject_grid').datagrid('reload');
				var rs = $.evalJSON(data);
				$.message(rs.msg);
			}
		}
	});
}

/**
 * 
 */
function deleteSubject() {
	var row = $('#subject_grid').datagrid('getSelected');
	var index = $('#subject_grid').datagrid('getRowIndex', row);
	if (row) {
		$.confirm('请确认是否删除选择的记录?', function() {
			var url = '${path}/subject/delete';
			$.post(url, {
				subjectId : row.subjectId
			},
			function(rs){
				if(rs.success){
					$.message(rs.msg || '删除科目成功！');
					$('#subject_grid').datagrid('reload');
				}else{
					$.message(rs.msg || '删除科目失败！');
				}
			},'json');
		});
	} else {
		$.alert('请选择要删除的记录！');
	}
}
</script>