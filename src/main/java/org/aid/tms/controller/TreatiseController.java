package org.aid.tms.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aid.commons.IPagination;
import org.aid.commons.LoginUserManger;
import org.aid.tms.model.TUser;
import org.aid.tms.model.Topic;
import org.aid.tms.model.Treatise;
import org.aid.tms.service.BaseService;
import org.aid.tms.service.ServiceException;
import org.aid.tms.service.TreatiseService;
import org.aid.tms.service.UserService;
import org.aid.tms.vo.QueryParam;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/treatise")
public class TreatiseController extends BaseController {
	@Autowired
	private TreatiseService treatiseService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/view")
	public String goTreatisePage(SecurityContextHolderAwareRequestWrapper request,Model model){
		String username = null;
		if(!request.isUserInRole("ROLE_ADMIN")){
			username = LoginUserManger.getLoginUsername();
		}
		List<TUser> users = userService.getUsersByUsername(username);
		model.addAttribute("users", users);
		return "treatise";
	}
	
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> getTreatiseList(Integer page,Integer rows,String queryParams){
		Map<String,Object> results = new HashMap<String, Object>();
		QueryParam[] params = null;
		if(StringUtils.isNotEmpty(queryParams)){
			try {
				params = new ObjectMapper().readValue(queryParams, QueryParam[].class);
			} catch (Throwable e) {
				System.out.println(e.getMessage());
			} 
		}
		IPagination<?> pagination = treatiseService.getListByPage(page,rows,params==null?null:Arrays.asList(params));
		
		results.put("total", pagination.getTotalCount());
		results.put("rows", pagination.getResults());
		return results;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public Map<String,Object> saveTreatise(Treatise treatise){
		Map<String,Object> results = new HashMap<String, Object>();
		if(treatise != null){
			if(treatise.getTreatiseId() == null){
				treatise.setCreateTime(new Timestamp(new Date().getTime()));
				treatise.setCreateUsername(LoginUserManger.getLoginUsername());
			}else{
				treatise.setLastUpdateTime(new Timestamp(new Date().getTime()));
				treatise.setLastUpdateUsername(LoginUserManger.getLoginUsername());
			}
		}
		
		try{
			treatiseService.save(treatise);
			results.put("success", true);
			results.put("msg","保存论文成功");
		}catch(ServiceException e){
			results.put("msg","保存论文失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	public Object getTreatise(Integer treatiseId){
		return treatiseService.getById(treatiseId);
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String,Object> deleteTreatiseById(Integer treatiseId){
		
		Map<String,Object> results = new HashMap<String, Object>();
		try{
			treatiseService.deleteById(treatiseId);
			results.put("success", true);
			results.put("msg","删除论文成功");
		}catch(ServiceException e){
			results.put("msg","删除论文失败，原因："+ e.getMessage());
		}
		return results;
	}
	
	@RequestMapping("/export/excel")
	public void exportTreatiseList(HttpServletRequest request,HttpServletResponse response,String queryParams) throws IOException{
		exportExcel(request, response, queryParams, new BuildExcelCallback() {
			
			@Override
			public BaseService getService() {
				return treatiseService;
			}
			
			@Override
			public String getFileName() {
				return "发表论文";
			}
			
			@Override
			public String[] getColumnHeads() {
				return new String[]{"姓名","论文题目","发表刊物","第一作者","合作者","年.卷.期.页码","年度"};
			}

			@Override
			public void fillRowData(HSSFRow row, Object d,CellStyle baseStyle) {
				Treatise t = (Treatise)d;
				setCellValue(row,0,t.getUser().getFullName(),baseStyle);
				setCellValue(row,1,t.getTreatiseTitle(),baseStyle);
				setCellValue(row,2,t.getPublication(),baseStyle);
				setCellValue(row,3,t.getFirstAuthor(),baseStyle);
				setCellValue(row,4,t.getCooperator(),baseStyle);
				setCellValue(row,5,t.getPosition(),baseStyle);
				setCellValue(row,6,t.getYear(),baseStyle);
			}
		});
	}
}
