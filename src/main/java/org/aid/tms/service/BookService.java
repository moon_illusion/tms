package org.aid.tms.service;

import org.aid.tms.dao.hibernate.BaseDao;
import org.aid.tms.dao.hibernate.BookDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService extends BaseService{
	
	@Autowired
	private BookDao bookDao;
	
	@Override
	public BaseDao getBaseDao() {
		return this.bookDao;
	}

	@Override
	public String getObjName() {
		return "论著";
	}
}
