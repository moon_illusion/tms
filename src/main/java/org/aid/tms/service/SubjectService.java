package org.aid.tms.service;

import org.aid.tms.dao.hibernate.BaseDao;
import org.aid.tms.dao.hibernate.SubjectDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubjectService extends BaseService {

	@Autowired
	private SubjectDao subjectDao;
	
	@Override
	public BaseDao getBaseDao() {
		return subjectDao;
	}

	@Override
	public String getObjName() {
		return "科目";
	}
}
