<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north'"
		style="height: 70px; vertical-align: middle; overflow: hidden;">
		<dl id="treatise_query_form" class="query-form">
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<dt>姓名</dt> 
			<dd>
				<select name="user_id" style="width: 110px;" _param="type:'this_'">
					<option value=""></option>
					<c:forEach items="${users}" var="user">
						<option value="${user.userId}">${user.fullName}</option>
					</c:forEach>
				</select>
			</dd>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user_id" _param="type:'this_'" value="${users[0].userId}" />
			</security:authorize>
			<dt>论文题目</dt>
			<dd>
				<input type="text" name="treatiseTitle" id="treatiseTitle" _param="relation:'like'" />
			</dd>
			<dt <security:authorize ifAllGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >发表刊物</dt> 
			<dd><input type="text" name="publication" _param="relation:'like'" /></dd>
			<dt <security:authorize ifNotGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >年度</dt> 
			<dd>
				<select name="year" style="width: 110px;">
					<option value=""></option>
					<c:forEach begin="2010" end="2020" var="item">
						<option value="${item}">${item}</option>
					</c:forEach>
				</select>
			</dd>
			<dd>
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="queryTreatise()">查询</a>
			<dd>
		</dl>
	</div>
	<div data-options="region:'center'">
		<table id="treatise_grid" class="easyui-datagrid"
			data-options="
				method:'post',
				url:'${path}/treatise/list',
				fit:true,
				border:false,
				singleSelect:true,
				idField:'treatiseId',
				pagination:true,
				toolbar:'#treatise_grid_toolbar',
				<security:authorize ifNotGranted="ROLE_ADMIN">
				queryParams:{
					queryParams:$.toJSON([{
						name:'user_id',
						type:'this_',
						relation:'=',
						value:'${users[0].userId}'
					}])
				},
				</security:authorize>
				onDblClickRow:function(rowIndex, rowData){
					showModifyTreatiseDialog(rowData);
				}
			">
			<thead>
				<tr>
					<th data-options="field:'user',width:100,align:'center',
						formatter:function(value,row,index){
							if(value){
								return value.fullName;
							}
							return '';
						}">姓名</th>
					<th data-options="field:'treatiseTitle',width:150,align:'center'">论文题目</th>
					<th data-options="field:'publication',width:150,align:'center'">发表刊物</th>
					<th data-options="field:'firstAuthor',width:150,align:'center'">第一作者</th>
					<th data-options="field:'cooperator',width:130,align:'center'">合作者</th>
					<th data-options="field:'position',width:180,align:'center'">年.卷.期.页码</th>
					<th data-options="field:'year',width:100,align:'center'">年度</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div id="treatise_grid_toolbar">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="showAddTreatiseDialog()">新增</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="showModifyTreatiseDialog()">修改</a>
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteTreatise()">删除</a>
	<a href="#" class="easyui-menubutton" data-options="menu:'#mmtreatise'">导出</a>
</div>

<div id="treatise_detail" class="easyui-dialog" style="width:400px;height:320px;padding:10px;"
	data-options="closed:true,cache:false,modal:true,buttons:'#treatise_detail_buttons'">
	<form id="treatise_detail_form" method="post">
	<input type="hidden" name="treatiseId" />
	<dl class="detail-form-list">
		<dt>姓名：</dt> 
		<dd>
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<select name="user" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach items="${users}" var="user">
					<option value="${user.userId}">${user.fullName}</option>
				</c:forEach>
			</select>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user" value="${users[0].userId}"/>
				<input type="text" name="fullName" readonly="readonly" value="${users[0].fullName}"/>
			</security:authorize>
		</dd>
		<dt>论文题目：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="treatiseTitle" data-options="required:true"/></dd>
		<dt>发表刊物：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="publication" data-options="required:true"/></dd>
		<dt>第一作者：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="firstAuthor" data-options="required:true"/></dd>
		<dt>合作者：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="cooperator" data-options="required:true"/></dd>
		<dt>年.卷.期.页码：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="position" data-options="required:true"/></dd>
		<dt>年度：</dt> 
		<dd>
			<select name="year" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach begin="2010" end="2020" var="item">
					<option value="${item}">${item}</option>
				</c:forEach>
			</select>
		</dd>
	</dl>
	<input type="hidden" name="createTime" />
	<input type="hidden" name="createUsername" />
</form>
	
</div>

<div id="treatise_detail_buttons">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="saveTreatise()">保存</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#treatise_detail').dialog('close');">取消</a>
</div>

<div id="mmtreatise" data-options="onClick:menuTreatiseHandler" style="width:150px;">
	<div data-options="name:'excel'">Excel</div>
</div>

<form id="treatise_export_form" method="post" action="${path}/treatise/export/excel">
	<security:authorize ifAllGranted="ROLE_ADMIN">
		<input type="hidden" id="treatise_query_params" name="queryParams">
	</security:authorize>
	<security:authorize ifNotGranted="ROLE_ADMIN">
		<input type="hidden" id="treatise_query_params" name="queryParams" value='[{"name":"user_id","type":"this_","relation":"=","value":"${users[0].userId}"}]'>
	</security:authorize>
</form>

<script type="text/javascript">

function menuTreatiseHandler(item){
	if(item.name=='excel'){
		$('#treatise_export_form').submit();
	}
}
	
function queryTreatise() {
	var params = [];
	
	$(':input','#treatise_query_form').each(function(i,v){
		var self = $(this);
		var val = self.val();
		if(val && val!='' ){
			var param = eval('({' + (self.attr('_param') || '') + '})') || {};
			if(!param.name || param.name==''){
				param.name = self.attr('name');
			}
			if(!param.relation || param.relation == ''){
				param.relation = '=';
			}
			param.value = val;
			params.push(param);
		}
	});
	
	var _query_params = $.toJSON(params);;
	
	$('#treatise_query_params').val(_query_params);
	$('#treatise_grid').datagrid('options').queryParams.queryParams =_query_params;
	$('#treatise_grid').datagrid('reload');
}

function showAddTreatiseDialog() {
	$("#treatise_detail")
		.dialog('setTitle', '新增论文')
		.dialog('open');
	$('#treatise_detail_form').form('clear');
	$('select','#treatise_detail_form').each(function(i,v){
		$(this).val($('option',this)[0].value);
	});
	
	$('input[name="user"]','#treatise_detail_form').val('${users[0].userId}');
	$('input[name="fullName"]','#treatise_detail_form').val('${users[0].fullName}');
}

function showModifyTreatiseDialog(rowData) {
	if (!rowData) {
		var row = $('#treatise_grid').datagrid('getSelected');
		if (row) {
			rowData = row;
		} else {
			$.alert('请选择要修改的记录！');
		}
	}

	if(rowData){
		$("#treatise_detail")
			.dialog('setTitle', '修改论文')
			.dialog('open');
		var url = '${path}/treatise/detail?treatiseId=' + rowData.treatiseId;
		$.get(url,function(rs){
			if(rs){
				var treatise = rs;
				var user = rs.user;
				if(user){
					treatise.user = user.userId;
					treatise.fullName = user.fullName;
					$('#treatise_detail_form').form('load',treatise);
				}
			}
		},'json');
	}
}

/**
 * 
 */
function saveTreatise() {
	var url = '${path}/treatise/save'
	
	$('#treatise_detail_form').form('submit', {
		url : url,
		onSubmit : function() {
			if($('input[name="user"]','#treatise_detail_form').val() == -1){
				return false;
			}
			if($('input[name="year"]','#treatise_detail_form').val() == -1){
				return false;
			}
			return $('#treatise_detail_form').form('validate');
		},
		success : function(data) {
			if(data){
				$('#treatise_detail').dialog('close');
				$('#treatise_grid').datagrid('reload');
				var rs = $.evalJSON(data);
				$.message(rs.msg);
			}
		}
	});
}

/**
 * 
 */
function deleteTreatise() {
	var row = $('#treatise_grid').datagrid('getSelected');
	var index = $('#treatise_grid').datagrid('getRowIndex', row);
	if (row) {
		$.confirm('请确认是否删除选择的记录?', function() {
			var url = '${path}/treatise/delete';
			$.post(url, {
				treatiseId : row.treatiseId
			},
			function(rs){
				if(rs.success){
					$.message(rs.msg || '删除论文成功！');
					$('#treatise_grid').datagrid('reload');
				}else{
					$.message(rs.msg || '删除论文失败！');
				}
			},'json');
		});
	} else {
		$.alert('请选择要删除的记录！');
	}
}
</script>