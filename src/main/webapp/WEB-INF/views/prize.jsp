<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north'"
		style="height: 70px; vertical-align: middle; overflow: hidden;">
		<dl id="prize_query_form" class="query-form">
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<dt>姓名</dt> 
			<dd>
				<select name="user_id" style="width: 110px;" _param="type:'this_'">
					<option value=""></option>
					<c:forEach items="${users}" var="user">
						<option value="${user.userId}">${user.fullName}</option>
					</c:forEach>
				</select>
			</dd>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user_id" _param="type:'this_'" value="${users[0].userId}" />
			</security:authorize>
			<dt>奖项名称</dt>
			<dd>
				<input type="text" name="prizeName" id="prizeName" _param="relation:'like'" />
			</dd>
			<dt <security:authorize ifAllGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >项目名称</dt> 
			<dd><input type="text" name="prizeProject" _param="relation:'like'" /></dd>
			<dt <security:authorize ifNotGranted="ROLE_ADMIN">style="clear: both;"</security:authorize> >年度</dt> 
			<dd>
				<select name="year" style="width: 110px;">
					<option value=""></option>
					<c:forEach begin="2010" end="2020" var="item">
						<option value="${item}">${item}</option>
					</c:forEach>
				</select>
			</dd>
			<dd>
				<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="queryPrize()">查询</a>
			<dd>
		</dl>
	</div>
	<div data-options="region:'center'">
		<table id="prize_grid" class="easyui-datagrid"
			data-options="
				method:'post',
				url:'${path}/prize/list',
				fit:true,
				border:false,
				singleSelect:true,
				idField:'prizeId',
				pagination:true,
				toolbar:'#prize_grid_toolbar',
				<security:authorize ifNotGranted="ROLE_ADMIN">
				queryParams:{
					queryParams:$.toJSON([{
						name:'user_id',
						type:'this_',
						relation:'=',
						value:'${users[0].userId}'
					}])
				},
				</security:authorize>
				onDblClickRow:function(rowIndex, rowData){
					showModifyPrizeDialog(rowData);
				}
			">
			<thead>
				<tr>
					<th data-options="field:'user',width:100,align:'center',
						formatter:function(value,row,index){
							if(value){
								return value.fullName;
							}
							return '';
						}">姓名</th>
					<th data-options="field:'prizeName',width:150,align:'center'">奖项名称</th>
					<th data-options="field:'prizeProject',width:150,align:'center'">项目名称</th>
					<th data-options="field:'presenter',width:130,align:'center'">主持人</th>
					<th data-options="field:'partner',width:130,align:'center'">合作者</th>
					<th data-options="field:'prizeTime',width:100,align:'center'">时间</th>
					<th data-options="field:'danwei',width:130,align:'center'">授予单位</th>
					<th data-options="field:'year',width:100,align:'center'">年度</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div id="prize_grid_toolbar">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="showAddPrizeDialog()">新增</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="showModifyPrizeDialog()">修改</a>
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deletePrize()">删除</a>
	<a href="#" class="easyui-menubutton" data-options="menu:'#mmprize'">导出</a>
</div>

<div id="prize_detail" class="easyui-dialog" style="width:420px;height:420px;padding:10px;"
	data-options="closed:true,cache:false,modal:true,buttons:'#prize_detail_buttons'">
	<form id="prize_detail_form" method="post">
	<input type="hidden" name="prizeId" />
	<dl class="detail-form-list">
		<dt>姓名：</dt> 
		<dd>
			<security:authorize ifAllGranted="ROLE_ADMIN">
			<select name="user" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach items="${users}" var="user">
					<option value="${user.userId}">${user.fullName}</option>
				</c:forEach>
			</select>
			</security:authorize>
			<security:authorize ifNotGranted="ROLE_ADMIN">
				<input type="hidden" name="user" value="${users[0].userId}"/>
				<input type="text" name="fullName" readonly="readonly" value="${users[0].fullName}"/>
			</security:authorize>
		</dd>
		<dt>奖项名称：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="prizeName" data-options="required:true"/></dd>
		<dt>项目名称：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="prizeProject" data-options="required:true"/></dd>
		<dt>主持人：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="presenter" data-options="required:true"/></dd>
		<dt>合作者：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="partner" data-options="required:true"/></dd>
		<dt>时间：</dt> 
		<dd><input type="text" class="easyui-datebox" name="prizeTime" data-options="required:true"/></dd>
		<dt>授予单位：</dt> 
		<dd><input type="text" class="easyui-validatebox" name="danwei" data-options="required:true"/></dd>
		<dt>年度：</dt> 
		<dd>
			<select name="year" style="width: 155px;">
				<option value="-1" selected="selected">请选择...</option>
				<c:forEach begin="2010" end="2020" var="item">
					<option value="${item}">${item}</option>
				</c:forEach>
			</select>
		</dd>
		<dt>备注：</dt> 
		<dd><textarea name="memo" rows="3" cols="25"></textarea></dd>
	</dl>
	<input type="hidden" name="createTime" />
	<input type="hidden" name="createUsername" />
</form>
	
</div>

<div id="prize_detail_buttons">
	<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="savePrize()">保存</a> 
	<a href="#"	class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" onclick="javascript:$('#prize_detail').dialog('close');">取消</a>
</div>

<div id="mmprize" data-options="onClick:menuPrizeHandler" style="width:150px;">
	<div data-options="name:'excel'">Excel</div>
</div>

<form id="prize_export_form" method="post" action="${path}/prize/export/excel">
	<security:authorize ifAllGranted="ROLE_ADMIN">
		<input type="hidden" id="prize_query_params" name="queryParams">
	</security:authorize>
	<security:authorize ifNotGranted="ROLE_ADMIN">
		<input type="hidden" id="prize_query_params" name="queryParams" value='[{"name":"user_id","type":"this_","relation":"=","value":"${users[0].userId}"}]'>
	</security:authorize>
</form>

<script type="text/javascript">

function menuPrizeHandler(item){
	if(item.name=='excel'){
		$('#prize_export_form').submit();
	}
}
	
function queryPrize() {
	var params = [];
	
	$(':input','#prize_query_form').each(function(i,v){
		var self = $(this);
		var val = self.val();
		if(val && val!='' ){
			var param = eval('({' + (self.attr('_param') || '') + '})') || {};
			if(!param.name || param.name==''){
				param.name = self.attr('name');
			}
			if(!param.relation || param.relation == ''){
				param.relation = '=';
			}
			param.value = val;
			params.push(param);
		}
	});
	
	var _query_params = $.toJSON(params);;
	
	$('#prize_query_params').val(_query_params);
	$('#prize_grid').datagrid('options').queryParams.queryParams =_query_params;
	$('#prize_grid').datagrid('reload');
}

function showAddPrizeDialog() {
	$("#prize_detail")
		.dialog('setTitle', '新增获奖情况')
		.dialog('open');
	$('#prize_detail_form').form('clear');
	$('select','#prize_detail_form').each(function(i,v){
		$(this).val($('option',this)[0].value);
	});
	
	$('input[name="user"]','#prize_detail_form').val('${users[0].userId}');
	$('input[name="fullName"]','#prize_detail_form').val('${users[0].fullName}');
}

function showModifyPrizeDialog(rowData) {
	if (!rowData) {
		var row = $('#prize_grid').datagrid('getSelected');
		if (row) {
			rowData = row;
		} else {
			$.alert('请选择要修改的记录！');
		}
	}

	if(rowData){
		$("#prize_detail")
			.dialog('setTitle', '修改获奖情况')
			.dialog('open');
		var url = '${path}/prize/detail?prizeId=' + rowData.prizeId;
		$.get(url,function(rs){
			if(rs){
				var prize = rs;
				var user = rs.user;
				if(user){
					prize.user = user.userId;
					prize.fullName = user.fullName;
					$('#prize_detail_form').form('load',prize);
				}
			}
		},'json');
	}
}

/**
 * 
 */
function savePrize() {
	var url = '${path}/prize/save'
	
	$('#prize_detail_form').form('submit', {
		url : url,
		onSubmit : function() {
			if($('input[name="user"]','#prize_detail_form').val() == -1){
				return false;
			}
			if($('input[name="year"]','#prize_detail_form').val() == -1){
				return false;
			}
			return $('#prize_detail_form').form('validate');
		},
		success : function(data) {
			if(data){
				$('#prize_detail').dialog('close');
				$('#prize_grid').datagrid('reload');
				var rs = $.evalJSON(data);
				$.message(rs.msg);
			}
		}
	});
}

/**
 * 
 */
function deletePrize() {
	var row = $('#prize_grid').datagrid('getSelected');
	var index = $('#prize_grid').datagrid('getRowIndex', row);
	if (row) {
		$.confirm('请确认是否删除选择的记录?', function() {
			var url = '${path}/prize/delete';
			$.post(url, {
				prizeId : row.prizeId
			},
			function(rs){
				if(rs.success){
					$.message(rs.msg || '删除获奖情况成功！');
					$('#prize_grid').datagrid('reload');
				}else{
					$.message(rs.msg || '删除获奖情况失败！');
				}
			},'json');
		});
	} else {
		$.alert('请选择要删除的记录！');
	}
}
</script>